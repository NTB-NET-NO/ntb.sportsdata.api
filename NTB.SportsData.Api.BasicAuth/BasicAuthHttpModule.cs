﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BasicAuthHttpModule.cs" company="">
//   
// </copyright>
// <summary>
//   The basic auth http module.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.BasicAuth
{
    using System;
    using System.Net.Http.Headers;
    using System.Security.Cryptography;
    using System.Security.Principal;
    using System.Text;
    using System.Threading;
    using System.Web;

    using NTB.SportsData.Domain;
    using NTB.SportsData.Data;
    
    using log4net;

    /// <summary>
    /// The basic auth http module.
    /// </summary>
    public class BasicAuthHttpModule : IHttpModule
    {
        #region Constants

        /// <summary>
        /// The realm.
        /// </summary>
        private const string Realm = "localhost";

        #endregion

        #region Static Fields

        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(BasicAuthHttpModule));

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create hash.
        /// </summary>
        /// <param name="apiUsername">
        /// The api username.
        /// </param>
        /// <param name="timestamp">
        /// The timestamp.
        /// </param>
        /// <param name="uri">
        /// The uri.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string CreateHash(string apiUsername, string timestamp, string uri, string password)
        {
            // string createStringToHash = apiUsername + timestamp + uri;
            // byte[] keyInByes = Encoding.UTF8.GetBytes(createStringToHash);
            string tosigndata = apiUsername + timestamp + uri;

            // string tosigndata = "test";
            MD5 alg = MD5.Create();
            var enc = new UTF8Encoding();

            byte[] hash = alg.ComputeHash(enc.GetBytes(tosigndata));
            string hashedString = Convert.ToBase64String(hash);

            /*
            ASCIIEncoding encoder = new ASCIIEncoding();
            string encodeString = apiUsername + timestamp + uri;
            byte[] data = encoder.GetBytes(encodeString);

            byte[] hashedBytes = MD5CryptoServiceProvider.Create().ComputeHash(data);
            // var hmacsha256 = new HMACSHA256(Encoding.UTF8.GetBytes(password));

            // hmacsha256.ComputeHash(data);

            // string hex = BitConverter.ToString(hmacsha256.Hash);

            string hashedString = BitConverter.ToString(hashedBytes).Replace("-", string.Empty).ToLower();

            */
            return hashedString;

            /*
            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] data = encoder.GetBytes(apiUsername + timestamp + uri);
            // HMACSHA1 hmac = new HMACSHA1(encoder.GetBytes(password));
            HMACMD5 hmac = new HMACMD5(encoder.GetBytes(password));
            CryptoStream cs = new CryptoStream(Stream.Null, hmac, CryptoStreamMode.Write);
            cs.Write(data, 0, data.Length);
            cs.Close();
            String hex = BitConverter.ToString(hmac.Hash);

            if ((hex.Length%2) == 1) hex += "0";

            return hex.Replace("-", "").ToLower();
             */
        }

        /// <summary>
        /// The pack h.
        /// </summary>
        /// <param name="hex">
        /// The hex.
        /// </param>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        public static byte[] PackH(string hex)
        {
            if ((hex.Length % 2) == 1)
            {
                hex += '0';
            }

            var bytes = new byte[hex.Length / 2];
            for (int i = 0; i < hex.Length; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            }

            return bytes;
        }

        /// <summary>
        /// The check signature.
        /// </summary>
        /// <param name="timestamp">
        /// The timestamp.
        /// </param>
        /// <param name="uri">
        /// The uri.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CheckSignature(string timestamp, string uri)
        {
            return false;
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// The init.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void Init(HttpApplication context)
        {
            Logger.Debug("Init");

            // Register event handlers
            context.AuthenticateRequest += OnApplicationAuthenticateRequest;
            context.EndRequest += OnApplicationEndRequest;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The authenticate user.
        /// </summary>
        /// <param name="credentials">
        /// The credentials.
        /// </param>
        /// <param name="signature">
        /// The signature.
        /// </param>
        /// <param name="timestamp">
        /// The timestamp.
        /// </param>
        /// <param name="uri">
        /// The uri.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool AuthenticateUser(string credentials, string signature, string timestamp, string uri)
        {
            Logger.Debug("Credentials: " + credentials);
            bool validated = false;
            try
            {
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                credentials = encoding.GetString(Convert.FromBase64String(credentials));

                int separator = credentials.IndexOf(':');
                string name = credentials.Substring(0, separator);
                string password = credentials.Substring(separator + 1);

                validated = CheckPassword(name, password);
                if (validated)
                {
                    // now we have validated the user, now we are to check the validity of the keys and stuff
                    // note: We have added this check here, because we only want to check this if the user is validated
                    // todo: get the real API-key/user from database so we don't expose information over network
                    string hash = CreateHash(name, timestamp, uri, password);

                    if (hash != signature)
                    {
                        validated = false;
                    }
                    else
                    {
                        var identity = new GenericIdentity(name);
                        SetPrincipal(new GenericPrincipal(identity, null));
                    }
                }
            }
            catch (FormatException)
            {
                // Credentials were not formatted correctly.
                validated = false;
            }

            return validated;
        }

        /// <summary>
        /// The check password.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool CheckPassword(string username, string password)
        {
            var userDataMapper = new Data.DataMappers.User.Api.UserDataMapper();
            Customer customer = userDataMapper.GetUser(username, password);
            return customer != null;
        }

        /// <summary>
        /// The on application authenticate request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void OnApplicationAuthenticateRequest(object sender, EventArgs e)
        {
            HttpRequest request = HttpContext.Current.Request;

            string signature = request.Headers["signature"];
            string timestamp = request.Headers["timestamp"];
            string uri = request.RawUrl;

            string authHeader = request.Headers["Authorization"];
            if (authHeader != null)
            {
                AuthenticationHeaderValue authHeaderVal = AuthenticationHeaderValue.Parse(authHeader);

                // RFC 2617 sec 1.2, "scheme" name is case-insensitive
                if (authHeaderVal.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase)
                    && authHeaderVal.Parameter != null)
                {
                    AuthenticateUser(authHeaderVal.Parameter, signature, timestamp, uri);
                }
            }
        }

        // If the request was unauthorized, add the WWW-Authenticate header 
        // to the response.
        /// <summary>
        /// The on application end request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void OnApplicationEndRequest(object sender, EventArgs e)
        {
            HttpResponse response = HttpContext.Current.Response;
            if (response.StatusCode == 401)
            {
                response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", Realm));
            }
        }

        /// <summary>
        /// The set principal.
        /// </summary>
        /// <param name="principal">
        /// The principal.
        /// </param>
        private static void SetPrincipal(IPrincipal principal)
        {
            Thread.CurrentPrincipal = principal;
            if (HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }
        }

        #endregion
    }
}