﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Input.Application.Interfaces.Common;
using NTB.SportsData.Input.Application.Interfaces.DataMappers;
using NTB.SportsData.Input.Domain.Classes;

namespace NTB.SportsData.Input.Application.DataMappers.Districts
{
    public class DistrictDataMapper : IRepository<District>, IDisposable, IDistrictDataMapper
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(DistrictDataMapper));

        public int InsertOne(District domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<District> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(District domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(District domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<District> GetAll()
        {
            List<District> districts = new List<District>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetDistricts", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return districts.AsQueryable();
                    }


                    while (sqlDataReader.Read())
                    {
                        var district = new District();
                        if (sqlDataReader["CountyId"] != DBNull.Value)
                        {
                            district.DistrictId = Convert.ToInt32(sqlDataReader["CountyId"]);
                        }

                        if (sqlDataReader["CountyName"] != DBNull.Value)
                        {
                            district.DistrictName = sqlDataReader["CountyName"].ToString();
                        }

                        if (sqlDataReader["District"] != DBNull.Value)
                        {
                            district.SecondaryDistrictId = Convert.ToInt32(sqlDataReader["District"]);
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            district.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        }

                        
                        districts.Add(district);
                    }
                    
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return districts.AsQueryable();
            }
        }

        public District Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<District> GetDistrictsByUserId(Guid userId)
        {
            List<District> districts = new List<District>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetDistrictsByUserid", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };


                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return districts;
                    }


                    while (sqlDataReader.Read())
                    {
                        var district = new District();
                        if (sqlDataReader["DistrictId"] != DBNull.Value)
                        {
                            district.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                            district.DistrictName = sqlDataReader["DistrictName"].ToString();
                        }

                        districts.Add(district);
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return districts;
            }
        }

        public void InsertUserDistrictMap(Guid userId, int districtId)
        {
            throw new NotImplementedException();
        }
    }
}