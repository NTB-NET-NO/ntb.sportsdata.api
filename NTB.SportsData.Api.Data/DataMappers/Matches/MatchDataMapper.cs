using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Data.Interfaces.Common;
using NTB.SportsData.Data.Interfaces.DataMappers;

namespace NTB.SportsData.Data.DataMappers.Matches
{
    public class MatchDataMapper : IRepository<Match>, IDisposable, IMatchDataMapper
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(MatchDataMapper));
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int InsertOne(Match domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertMatches", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", domainobject.MatchId));
                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@AwayTeamId", domainobject.AwayTeamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@AwayTeamName", domainobject.AwayTeam));
                    sqlCommand.Parameters.Add(new SqlParameter("@HomeTeamId", domainobject.HomeTeamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@HomeTeamName", domainobject.HomeTeam));
                    sqlCommand.Parameters.Add(new SqlParameter("@MatchDate", domainobject.MatchDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@MatchTime", domainobject.MatchStartTime));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return 1;
            }
        }

        public void InsertAll(List<Match> domainobject)
        {
            foreach (Match match in domainobject)
            {
                InsertOne(match);
            }
        }

        public void Update(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Match> GetAll()
        {
            var matches = new List<Match>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetMatches", sqlConnection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }


                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();


                    if (sqlDataReader.Read())
                    {
                        var match = new Match
                            {
                                MatchId = (int) sqlDataReader["MatchId"],
                                SportId = (int) sqlDataReader["SportId"],
                                HomeTeam = sqlDataReader["HomeTeam"].ToString(),
                                AwayTeam = sqlDataReader["AwayTeam"].ToString(),
                                MatchDate = Convert.ToDateTime(sqlDataReader["Date"].ToString()),
                                TournamentId = (int) sqlDataReader["TournamentId"]
                            };
                        matches.Add(match);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return matches.AsQueryable();
            }
        }

        public Match Get(int id)
        {
            var match = new Match();

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetMatchById", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    sqlCommand.Parameters.Add(new SqlParameter("MatchId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }


                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    
                    if (!sqlDataReader.HasRows)
                    {
                        match.MatchId = 0;
                        return match;
                    }

                    while (sqlDataReader.Read())
                    {
                        Logger.InfoFormat("Getting information on match with MatchId: {0}", id);
                        match.MatchId = Convert.ToInt32(sqlDataReader["MatchId"]);
                        match.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        match.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);

                        if (sqlDataReader["UserId"] != DBNull.Value)
                        {
                            match.UserId = new Guid(sqlDataReader["UserId"].ToString());  
                        } 

                        match.HomeTeam = sqlDataReader["HomeTeamName"].ToString();
                        match.HomeTeamId = Convert.ToInt32(sqlDataReader["AwayTeamId"]);
                        match.AwayTeam = sqlDataReader["AwayTeamName"].ToString();
                        match.AwayTeamId = Convert.ToInt32(sqlDataReader["AwayTeamId"]);
                        match.MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"]);
                        if (sqlDataReader["MatchTime"] != DBNull.Value)
                        {
                            match.MatchStartTime = Convert.ToInt32(sqlDataReader["MatchTime"]);
                        }
                        match.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                    }

                } catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return match;
            }
        }

        public List<Match> GetMatchesByTournamentId(int tournamentId)
        {
            List<Match> matches = new List<Match>();

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetMatchesByTournamentId", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    sqlCommand.Parameters.Add(new SqlParameter("Tournamentid", tournamentId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var match = new Match();
                            match.MatchId = (int) sqlDataReader["MatchId"];
                            match.SportId = (int) sqlDataReader["SportId"];
                            match.HomeTeam = sqlDataReader["HomeTeam"].ToString();
                            match.AwayTeam = sqlDataReader["AwayTeam"].ToString();
                            match.MatchDate = Convert.ToDateTime(sqlDataReader["Date"].ToString());
                            match.TournamentId = (int) sqlDataReader["TournamentId"];

                            matches.Add(match);
                        }
                    }
                } catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return matches;
            }
        }

        public List<Match> GetLocalMatchesByDistrictId(int districtId)
        {
            List<Match> matches = new List<Match>();

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetTodaysMatchesByDistrict", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("DistrictId", districtId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return matches;
                    }
                    
                    while (sqlDataReader.Read())
                    {
                        var match = new Match
                            {
                                MatchId = Convert.ToInt32(sqlDataReader["MatchId"]),
                                SportId = Convert.ToInt32(sqlDataReader["SportId"]),
                                HomeTeam = sqlDataReader["HomeTeamName"].ToString(),
                                HomeTeamId = Convert.ToInt32(sqlDataReader["HomeTeamId"]),
                                AwayTeam = sqlDataReader["AwayTeamName"].ToString(),
                                AwayTeamId = Convert.ToInt32(sqlDataReader["AwayTeamId"]),
                                MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"].ToString()),
                                MatchStartTime = Convert.ToInt32(sqlDataReader["MatchTime"]),
                                TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]),
                                TournamentName = sqlDataReader["TournamentName"].ToString(),
                                MatchComment = sqlDataReader["MatchComment"].ToString()
                            };
                        // match.HomeTeamId = Convert.ToInt32(sqlDataReader["HomeTeamGoal"]);
                        // match.AwayTeamId = Convert.ToInt32(sqlDataReader["AwayTeamGoal"]);

                        matches.Add(match);
                    }
                    
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return matches;
            }
        }

        public MatchComment GetMatchComment(int matchId)
        {
            MatchComment matchComment = new MatchComment();

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetMatchCommentByMatchId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("MatchId", matchId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return matchComment;
                    }

                    while (sqlDataReader.Read())
                    {
                        matchComment.Id = matchId;
                        matchComment.Comment = sqlDataReader["MatchComment"].ToString();
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return matchComment;
            }
        }

        public void StoreMatchComment(int matchId, string matchComment)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_StoreMatchComment", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("MatchId", matchId));
                    sqlCommand.Parameters.Add(new SqlParameter("MatchComment", matchComment));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}