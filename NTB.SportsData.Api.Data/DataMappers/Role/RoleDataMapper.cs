﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Data.Interfaces.Common;
using NTB.SportsData.Data.Interfaces.DataMappers;

namespace NTB.SportsData.Data.DataMappers.Role
{
    public class RoleDataMapper : IRoleDataMapper
    {

        readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IQueryable<Domain.Classes.Role> GetRoles()
        {
            // now that we have the user, we can insert the rest of the information
            var roles = new List<Domain.Classes.Role>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetUserRoles", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return roles.AsQueryable();
                    }

                    while (sqlDataReader.Read())
                    {
                        var role = new Domain.Classes.Role();

                        role.Id = sqlDataReader["Id"].ToString();
                        role.Name = sqlDataReader["Name"].ToString();

                        roles.Add(role);
                    }
                    
                    return roles.AsQueryable();
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message);
                    _logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        _logger.Error(exception.InnerException.Message);
                        _logger.Error(exception.InnerException.StackTrace);
                    }

                    return roles.AsQueryable();
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertRole(Domain.Classes.Role user)
        {
            throw new NotImplementedException();
        }

        public void UpdateRole(Domain.Classes.Role user)
        {
            throw new NotImplementedException();
        }

        public void DeleteRole(Domain.Classes.Role user)
        {
            throw new NotImplementedException();
        }

        public Domain.Classes.Role GetRoleByUserId(string userId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var role = new Domain.Classes.Role();
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetRoleByUserId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));


                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return role;
                    }

                    while (sqlDataReader.Read())
                    {
                        role.Id = sqlDataReader["Id"].ToString();
                        role.Name = sqlDataReader["Name"].ToString();
                    }

                    return role;
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message);
                    _logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        _logger.Error(exception.InnerException.Message);
                        _logger.Error(exception.InnerException.StackTrace);
                    }

                    return role;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public Domain.Classes.Role GetRoleByUserName(string userName)
        {
            // now that we have the user, we can insert the rest of the information
            
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var role = new Domain.Classes.Role();
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetRoleByUserName", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserName", userName));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return role;
                    }

                    while (sqlDataReader.Read())
                    {
                        role.Id = sqlDataReader["Id"].ToString();
                        role.Name = sqlDataReader["Name"].ToString();
                    }

                    return role;
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message);
                    _logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        _logger.Error(exception.InnerException.Message);
                        _logger.Error(exception.InnerException.StackTrace);
                    }

                    return role;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void AddUserToRole(ApplicationUser user, string roleName)
        {
            // now that we have the user, we can insert the rest of the information

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var role = new Domain.Classes.Role();
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_AddUserToRole", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", user.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@RoleName", roleName));

                    sqlCommand.ExecuteNonQuery();


                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message);
                    _logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        _logger.Error(exception.InnerException.Message);
                        _logger.Error(exception.InnerException.StackTrace);
                    }

                    
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void RemoveUserFromRole(ApplicationUser user, string roleId)
        {
            
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var role = new Domain.Classes.Role();
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_RemoveUserFromRole", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", user.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@RoleId", roleId));

                    sqlCommand.ExecuteNonQuery();


                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message);
                    _logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        _logger.Error(exception.InnerException.Message);
                        _logger.Error(exception.InnerException.StackTrace);
                    }

                    
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}