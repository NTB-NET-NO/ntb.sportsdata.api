using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsData.Input.Application.Interfaces.Common;
using NTB.SportsData.Input.Application.Interfaces.DataMappers;
using NTB.SportsData.Input.Domain.Classes;

namespace NTB.SportsData.Input.Application.DataMappers.Sports
{
    public class SportDataMapper : IRepository<Sport>, IDisposable, ISportDataMapper
    {
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int InsertOne(Sport domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Sport> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Sport domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Sport domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_DeleteSport", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.Id));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        public IQueryable<Sport> GetAll()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // DONE: Move code to SportsData Stored Procedures
                var sqlCommand = new SqlCommand("SportsData_GetSports", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                var sports = new List<Sport>();
                while (sqlDataReader.Read())
                {
                    var sport = new Sport();
                    if (sqlDataReader["Sport"] != DBNull.Value)
                    {
                        sport.Name = sqlDataReader["Sport"].ToString();
                    }

                    if (sqlDataReader["SportId"] != DBNull.Value)
                    {
                        sport.Id = Convert.ToInt32(sqlDataReader["SportId"].ToString());
                    }

                    if (sqlDataReader["SingleSport"] != DBNull.Value)
                    {
                        sport.HasIndividualResult = (int) sqlDataReader["SingleSport"];
                    }

                    if (sqlDataReader["TeamSport"] != DBNull.Value)
                    {
                        sport.HasTeamResults = (int) sqlDataReader["TeamSport"];
                    }

                    if (sqlDataReader["OrgId"] != DBNull.Value)
                    {
                        sport.OrgId = (int) sqlDataReader["OrgId"];
                    }

                    if (sqlDataReader["OrgName"] != DBNull.Value)
                    {
                        sport.OrgName = (string) sqlDataReader["OrgName"];
                    }
                    sports.Add(sport);
                }

                sqlDataReader.Close();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return sports.AsQueryable();
            }
        }

        public Sport Get(int id)
        {
            // returning only the selected sport
            return GetAll().Single(s => s.Id == id);
        }


        public int GetSportIdFromOrgId(int orgId)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand("SportsData_GetSportIdByOrgId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@OrgId", orgId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                int sportId = 0;

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        sportId = Convert.ToInt32(sqlDataReader["SportId"]);
                    }
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return sportId;
            }
        }

        public int GetSportIdBySeasonId(int seasonId)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand("SportsData_GetSportIdBySeasonId", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", seasonId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                int sportId = 0;

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        sportId = Convert.ToInt32(sqlDataReader["SportId"]);
                    }
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return sportId;
            }
        }
    }
}