﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Data.DataMappers.User;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Data.Interfaces.Common;
using NTB.SportsData.Data.Interfaces.DataMappers;

namespace NTB.SportsData.Data.DataMappers.UserProfiles
{
    public class UserProfileDataMapper : IRepository<UserProfile>, IUserProfileDataMapper, IDisposable
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof (UserProfileDataMapper));

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int InsertOne(UserProfile domainobject)
        {
            Logger.Debug("Inserting one object: " + domainobject.FirstName + " " + domainobject.LastName);

            // throw new NotImplementedException();

            // First we must create the user with the data provided using the MembershipUser thing
            var userDataMapper = new UserProfileDataMapper();
            
            userDataMapper.InsertProfile(domainobject);
            //var user = Membership.CreateUser(domainobject.UserName, domainobject.PassWord, domainobject.Email);
            //user.LastActivityDate = DateTime.Now.AddMinutes(-60);
            //Membership.UpdateUser(user);

            string userId = string.Empty;
            //var membershipUser = Membership.GetUser(user.UserName);
            //if (membershipUser != null)
            //{
            //    if (membershipUser.ProviderUserKey != null)
            //    {
            //        userId = membershipUser.ProviderUserKey.ToString();
            //    }
            //}

            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertUserProfile", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    sqlCommand.Parameters.Add(new SqlParameter("@FirstName", domainobject.FirstName));

                    sqlCommand.Parameters.Add(new SqlParameter("@LastName", domainobject.LastName));

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Dispose();

                    return 1;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }

                    return 0;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertAll(List<UserProfile> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(UserProfile domainobject)
        {
            // First we must create the user with the data provided using the MembershipUser thing

            var userId = new Guid();
            //var membershipUser = Membership.GetUser(domainobject.UserId);
            
            //if (membershipUser != null)
            //{
            //    if (membershipUser.ProviderUserKey != null)
            //    {
            //        userId = new Guid(membershipUser.ProviderUserKey.ToString());
            //    }
            //    membershipUser.Email = domainobject.Email;
                
            //    Membership.UpdateUser(membershipUser);
            //}

            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_UpdateUserProfile", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    sqlCommand.Parameters.Add(new SqlParameter("@FirstName", domainobject.FirstName));

                    sqlCommand.Parameters.Add(new SqlParameter("@LastName", domainobject.LastName));

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Dispose();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }

                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void Delete(UserProfile domainobject)
        {
            // First we must create the user with the data provided using the MembershipUser thing

            string userId = string.Empty;
            //var membershipUser = Membership.GetUser(domainobject.UserId);
            //if (membershipUser != null)
            //{
            //    if (membershipUser.ProviderUserKey != null)
            //    {
            //        userId = membershipUser.ProviderUserKey.ToString();
            //    }
            //}

            //// then we delete the user
            //Membership.DeleteUser(domainobject.UserName);

            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteUserProfile", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Dispose();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }

                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public IQueryable<UserProfile> GetAll()
        {
            var userProfiles = new List<UserProfile>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetUserProfiles", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new List<UserProfile>().AsQueryable();
                    }

                    
                    while (sqlDataReader.Read())
                    {
                        var userProfile = new UserProfile();
                        if (sqlDataReader["Firstname"] != DBNull.Value)
                        {
                            userProfile.FirstName = sqlDataReader["Firstname"].ToString();
                        }

                        if (sqlDataReader["Lastname"] != DBNull.Value)
                        {
                            userProfile.LastName = sqlDataReader["Lastname"].ToString();
                        }

                        if (sqlDataReader["UserId"] != DBNull.Value)
                        {
                            userProfile.UserId = new Guid(sqlDataReader["UserId"].ToString());
                        }

                        userProfiles.Add(userProfile);
                    }
                    
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            return userProfiles.AsQueryable();
        }

        public UserProfile Get(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<UserProfile> GetUserProfiles()
        {
            throw new NotImplementedException();
        }

        public UserProfile GetUserProfile(Guid userId)
        {
            Logger.Debug("Getting UserProfile for userid " + userId);
            var userProfile = new UserProfile();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetUserProfile", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("UserId", userId));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new UserProfile();
                    }


                    while (sqlDataReader.Read())
                    {

                        if (sqlDataReader["Firstname"] != DBNull.Value)
                        {
                            userProfile.FirstName = sqlDataReader["Firstname"].ToString();
                        }

                        if (sqlDataReader["Email"] != DBNull.Value)
                        {
                            userProfile.Email = sqlDataReader["Email"].ToString();
                        }

                        if (sqlDataReader["Lastname"] != DBNull.Value)
                        {
                            userProfile.LastName = sqlDataReader["Lastname"].ToString();
                        }

                        if (sqlDataReader["UserId"] != DBNull.Value)
                        {
                            userProfile.UserId = new Guid(sqlDataReader["UserId"].ToString());
                        }

                        if (sqlDataReader["WorkRoleId"] != DBNull.Value)
                        {
                            userProfile.WorkRole.Id = Convert.ToInt32(sqlDataReader["WorkRoleId"]);
                        }
                    }
                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            return userProfile;
        }

        internal void InsertProfile(UserProfile userProfile)
        {

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertUserProfile", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("UserId", userProfile.UserId));

                    sqlCommand.Parameters.Add(new SqlParameter("FirstName", userProfile.FirstName));

                    sqlCommand.Parameters.Add(new SqlParameter("LastName", userProfile.LastName));

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}