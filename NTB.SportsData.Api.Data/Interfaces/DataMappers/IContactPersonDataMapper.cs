﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface IContactPersonDataMapper
    {
        List<ContactPerson> GetContactPersonByTeamId(int teamId);
        void ActivateDeactivateContactPerson(int personId);
        void DeleteContactTeamMap(int teamId, int contactId);
        void InsertContactTeamMap(int teamId, int contactId);
    }
}
