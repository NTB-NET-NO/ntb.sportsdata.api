﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface IDistrictDataMapper
    {
        List<District> GetDistrictsByUserId(Guid userId);
        void InsertUserDistrictMap(Guid userId, int districtId);
    }
}
