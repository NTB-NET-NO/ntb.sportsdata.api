﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface IMatchDataMapper
    {
        List<Match> GetMatchesByTournamentId(int tournamentId);
        List<Match> GetLocalMatchesByDistrictId(int districtId);
        MatchComment GetMatchComment(int matchId);
        void StoreMatchComment(int matchId, string matchComment);
    }
}
