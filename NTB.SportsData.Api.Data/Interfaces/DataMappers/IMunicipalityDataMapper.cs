﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface IMunicipalityDataMapper
    {
        List<Municipality> GetMunicipalitiesByJobId(int jobId);
        List<Municipality> GetMunicipalitiesBySportId(int sportId);
        void StoreMunicipalities(List<Municipality> municipalities);
        void DeleteAll();

    }
}
