﻿using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface IPopulateDataMapper
    {
        DataBasePopulate GetDataBaseUpdated();
    }
}
