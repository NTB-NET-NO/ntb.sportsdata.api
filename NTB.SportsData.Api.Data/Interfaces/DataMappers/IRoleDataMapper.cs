﻿using System.Linq;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface IRoleDataMapper
    {
        IQueryable<Role> GetRoles();
        void InsertRole(Role user);
        void UpdateRole(Role user);
        void DeleteRole(Role user);
        Role GetRoleByUserId(string userId);
        Role GetRoleByUserName(string userName);
        void AddUserToRole(ApplicationUser user, string roleName);
        void RemoveUserFromRole(ApplicationUser user, string roleName);
    }
}
