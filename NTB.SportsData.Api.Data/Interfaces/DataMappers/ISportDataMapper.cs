﻿namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface ISportDataMapper
    {
        int GetSportIdBySeasonId(int seasonId);
        int GetSportIdFromOrgId(int orgId);
    }
}
