﻿namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface ITeamDataMapper
    {
        void DeleteTeamContactMap(int teamId, int contactId);
        void InsertTeamContactMap(int teamId, int contactId);
        
    }
}
