﻿using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface ITeamRoleDataMapper
    {
        void DeleteTeamRole(int functionId);
        void InsertTeamRole(Function function);
        Function GetTeamRoleById(int functionId);
    }
}
