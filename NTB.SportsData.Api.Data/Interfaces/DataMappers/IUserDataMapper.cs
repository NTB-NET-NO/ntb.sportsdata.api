﻿using System.Linq;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface IUserDataMapper
    {
        IQueryable<ApplicationUser> GetUsers();
        void InsertUser(ApplicationUser user);
        void UpdateUser(ApplicationUser user);
        void DeleteUser(ApplicationUser user);
        ApplicationUser GetUserByUserId(string userId);
        ApplicationUser GetUserByUserName(string userName);
        ApplicationUser GetUserByEmail(string email);
        bool IsUserInRoleByUserId(string userId);
    }
}
