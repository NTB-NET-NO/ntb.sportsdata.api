﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface IUserDistrictDataMapper
    {
        void InsertUserMap(UserDistrictMapper domainobject);
        bool GetUserByUserIdAndDistrictId(Guid userId, int districtId);
        List<DateTime> GetAllUserSessionsByUserId(Guid userId);
        void DeleteMappingByDate(DateTime dateTime);
    }
}
