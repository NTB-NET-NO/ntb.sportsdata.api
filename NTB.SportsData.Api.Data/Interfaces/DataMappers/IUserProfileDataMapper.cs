﻿using System;
using System.Linq;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface IUserProfileDataMapper
    {
        IQueryable<UserProfile> GetUserProfiles();
        UserProfile GetUserProfile(Guid userId);
    }
}
