﻿using System;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface IWorkRoleDataMapper
    {
        void DeleteAll();
        WorkRole GetWorkRoleByUserId(Guid userId);
    }
}
