﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces
{
    public interface IMatchDataMapper
    {
        List<Match> GetMatchesByTournamentId(int tournamentId);
        Match GetMatchById(int id);
        List<Match> GetTodaysMatchesByCustomerId(int customerId);
    }
}
