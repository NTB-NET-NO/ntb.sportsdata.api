﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces
{
    public interface ITeamDataMapper
    {
        List<Domain.Classes.Team> GetTournamentTeams(Tournament tournament);
    }
}
