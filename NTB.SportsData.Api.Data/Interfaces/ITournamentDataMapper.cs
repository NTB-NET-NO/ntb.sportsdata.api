﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces
{
    public interface ITournamentDataMapper
    {
        List<Tournament> GetTournamentsByCustomerId(int customerId);
        Tournament GetTournamentById(int id);
    }
}
