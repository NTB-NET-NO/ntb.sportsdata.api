﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces
{
    public interface ITournamentRemoteDataMapper
    {
        List<TournamentTable> GetTournamentStandings(Tournament tournament);
    }
}
