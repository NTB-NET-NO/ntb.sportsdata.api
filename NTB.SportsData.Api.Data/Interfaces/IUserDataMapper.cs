﻿using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces
{
    public interface IUserDataMapper
    {
        Customer GetUser(string username, string password);
    }
}
