﻿using System.Collections.Generic;
using NTB.SportsData.Data.Interfaces;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Interfaces;
using NTB.SportsData.Facade.NFF.ApiAccess;
using NTB.SportsData.Facade.NIF.ApiAccess;

namespace NTB.SportsData.Data.Team
{
    public class TeamRemoteDataMapper : ITeamDataMapper
    {
        public List<Domain.Classes.Team> GetTournamentTeams(Tournament tournament)
        {

            IApiFacade apifacade;
            if (tournament.SportId == 16)
            {
                apifacade = new SoccerApiFacade();
            }
            else
            {
                apifacade = new SportApiFacade();
            }

            return apifacade.GetTournamentTeams(tournament.TournamentId);
        }
    }
}
