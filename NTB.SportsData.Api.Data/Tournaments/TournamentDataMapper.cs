﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;
using NTB.SportsData.Data.Interfaces;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Tournaments
{
    public class TournamentDataMapper : ITournamentDataMapper
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentDataMapper));

        public Tournament GetTournamentById(int id)
        {
            var tournament = new Tournament();
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    var sqlCommand = new SqlCommand("SportsDataApi_GetTournamentByTournamentId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", id));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return tournament;
                    }

                    
                    while (sqlDataReader.Read())
                    {
                        tournament.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        tournament.TournamentName = sqlDataReader["TournamentName"].ToString();
                        tournament.SportName = sqlDataReader["Sport"].ToString();
                        tournament.SportId = sqlDataReader["SportId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["SportId"]);
                        tournament.OrganizationId = sqlDataReader["OrgId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["OrgName"].ToString());
                        tournament.OrganizationName = sqlDataReader["OrgName"] == DBNull.Value ? string.Empty : sqlDataReader["OrgName"].ToString();
                        tournament.OrganizationNameShort = sqlDataReader["OrgNameShort"].ToString();
                        tournament.SeasonId = sqlDataReader["SeasonId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["SeasonId"]);
                        tournament.SeasonName = sqlDataReader["SeasonName"].ToString();
                        tournament.StartDate = Convert.ToDateTime(sqlDataReader["SeasonStartDate"]);
                        tournament.EndDate = Convert.ToDateTime(sqlDataReader["SeasonEndDate"]);
                        tournament.AgeCategoryDefinition = sqlDataReader["AgeCategoryDefinition"] == DBNull.Value ? string.Empty : sqlDataReader["AgeCategoryDefinition"].ToString();
                        tournament.DisciplineId = sqlDataReader["DisciplineId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DisciplineId"]);
                        tournament.DisciplineName = sqlDataReader["Discipline"].ToString();
                    }

                    return tournament;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);

                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return tournament;
            }
        }

        
        public List<Tournament> GetTournamentsByCustomerId(int customerId)
        {
            var tournaments = new List<Tournament>();

            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    var sqlCommand = new SqlCommand("SportsDataApi_GetTournamentsByCustomerId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return tournaments;
                    }

                    
                    while (sqlDataReader.Read())
                    {
                        var tournament = new Tournament();

                        tournament.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        tournament.TournamentName = sqlDataReader["TournamentName"].ToString();
                        tournament.SportName = sqlDataReader["Sport"].ToString();
                        tournament.SportId = sqlDataReader["SportId"] == DBNull.Value
                            ? 0
                            : Convert.ToInt32(sqlDataReader["SportId"]);
                        tournament.SeasonId = sqlDataReader["SeasonId"] == DBNull.Value
                            ? 0
                            : Convert.ToInt32(sqlDataReader["SeasonId"]);
                        tournament.SeasonName = sqlDataReader["SeasonName"].ToString();
                        tournament.StartDate = Convert.ToDateTime(sqlDataReader["SeasonStartDate"]);
                        tournament.EndDate = Convert.ToDateTime(sqlDataReader["SeasonEndDate"]);
                        tournament.AgeCategoryDefinition = sqlDataReader["AgeCategoryDefinition"] == DBNull.Value
                            ? string.Empty
                            : sqlDataReader["AgeCategoryDefinition"].ToString();
                        tournament.DisciplineId = sqlDataReader["DisciplineId"] == DBNull.Value
                            ? 0
                            : Convert.ToInt32(sqlDataReader["DisciplineId"]);
                        tournament.DisciplineName = sqlDataReader["Discipline"] == DBNull.Value
                            ? string.Empty
                            : sqlDataReader["Discipline"].ToString();
                        tournament.OrganizationId = sqlDataReader["OrgId"] == DBNull.Value
                            ? 0
                            : Convert.ToInt32(sqlDataReader["OrgId"].ToString());
                        tournament.OrganizationName = sqlDataReader["OrgName"] == DBNull.Value
                            ? string.Empty
                            : sqlDataReader["OrgName"].ToString();
                        tournament.OrganizationNameShort = sqlDataReader["OrgNameShort"] == DBNull.Value
                            ? string.Empty
                            : sqlDataReader["OrgNameShort"].ToString();
                          

                        tournaments.Add(tournament);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }

                    return tournaments;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return tournaments;
        }
    }
}
