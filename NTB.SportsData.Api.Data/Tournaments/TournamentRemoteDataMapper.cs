﻿using System.Collections.Generic;
using NTB.SportsData.Data.Interfaces;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Interfaces;
using NTB.SportsData.Facade.NFF.ApiAccess;
using NTB.SportsData.Facade.NIF.ApiAccess;

namespace NTB.SportsData.Data.Tournaments
{
    public class TournamentRemoteDataMapper : ITournamentRemoteDataMapper
    {
        public List<TournamentTable> GetTournamentStandings(Tournament tournament)
        {
            IApiFacade facade;
            if (tournament.SportId == 16)
            {
                facade = new SoccerApiFacade();
            }
            else
            {
                facade = new SportApiFacade();
            }

            return facade.GetTournamentStanding(tournament);
        }
    }
}
