﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;
using NTB.SportsData.Data.Interfaces;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Users
{
    public class UserDataMapper : IUserDataMapper
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(UserDataMapper));

        public Customer GetUser(string username, string password)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    var sqlCommand = new SqlCommand("SportsDataApi_GetUserByCredentials", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@Username", username));
                    sqlCommand.Parameters.Add(new SqlParameter("@Password", password));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    // var customer = new Customer();
                    while (sqlDataReader.Read())
                    {
                        return new Customer
                        {
                            CustomerId = Convert.ToInt32(sqlDataReader["Customerid"]),
                            Name = sqlDataReader["CustomerName"].ToString()
                        };
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);

                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return null;
        }
    }
}
