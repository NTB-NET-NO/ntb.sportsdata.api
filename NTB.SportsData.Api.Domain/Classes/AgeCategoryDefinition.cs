// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AgeCategoryDefinition.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The age category definition.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Domain.Classes
{
    /// <summary>
    /// The age category definition.
    /// </summary>
    public class AgeCategoryDefinition
    {
        /// <summary>
        /// Gets or sets the definition id.
        /// </summary>
        public int DefinitionId { get; set; }

        /// <summary>
        /// Gets or sets the definition name.
        /// </summary>
        public string DefinitionName { get; set; }
    }
}