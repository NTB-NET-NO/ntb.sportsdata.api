﻿
namespace NTB.SportsData.Api.Domain.Classes
{
    public class ApiUser
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
