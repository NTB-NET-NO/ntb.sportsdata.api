﻿
namespace NTB.SportsData.Api.Domain.Classes
{
    public class Club
    {
        public int ClubId { get; set; }
        public string ClubName { get; set; }
    }
}
