﻿namespace NTB.SportsData.Api.Domain.Classes
{
    public class ContactPerson
    {
        public int PersonId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int RoleId { get; set; }

        public string RoleName { get; set; }

        public string Email { get; set; }

        public int TeamId { get; set; }

        public string TeamName { get; set; }

        public string MobilePhone { get; set; }

        public string OfficePhone { get; set; }

        public string HomePhone { get; set; }

        public int Active { get; set; }

        public string Gender { get; set; }
    }
}
