// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer_Actions.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The customer actions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Domain.Classes
{
    /// <summary>
    /// The customer actions.
    /// </summary>
    public class CustomerActions
    {
        /// <summary>
        /// Gets or sets the customer action id.
        /// </summary>
        public int CustomerActionId { get; set; }

        /// <summary>
        /// Gets or sets the customer action.
        /// </summary>
        public string CustomerAction { get; set; }
    }
}