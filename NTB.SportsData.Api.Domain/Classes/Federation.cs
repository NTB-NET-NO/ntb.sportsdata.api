using System.Collections.Generic;

namespace NTB.SportsData.Api.Domain.Classes
{
    public class Federation
    {
        /// <summary>
        ///     Gets or sets the federation id.
        /// </summary>
        public int FederationId { get; set; }

        /// <summary>
        ///     Gets or sets the federation name.
        /// </summary>
        public string FederationName { get; set; }

        /// <summary>
        ///     Gets or sets the federation disciplines
        /// </summary>
        public List<FederationDiscipline> FederationDisciplines { get; set; }

        /// <summary>
        ///     Gets or sets the has team result property
        /// </summary>
        public int HasTeamResults { get; set; }

        /// <summary>
        ///     Gets or sets the has individual results property
        /// </summary>
        public int HasIndividualResults { get; set; }

        /// <summary>
        ///     Gets or sets the branch code
        /// </summary>
        public string BranchCode { get; set; }

        /// <summary>
        ///     Gets or sets the branch id
        /// </summary>
        public int? BranchId { get; set; }

        /// <summary>
        ///     Gets or sets the branch name
        /// </summary>
        public string BranchName { get; set; }
    }
}