﻿namespace NTB.SportsData.Api.Domain.Classes.JSON
{
    public class Competition
    {
        public int Competitionid { get; set; }

        public int CompetitionName { get; set; }

        public string CompetitionStartDate { get; set; }

        public string CompetitionEndDate { get; set; }

        public int SportId { get; set; }

        public string SportName { get; set; }

        public int? AgeCategoryId { get; set; }

        public string AgeCategory { get; set; }

        public int SeasonId { get; set; }

        public string SeasonName { get; set; }

        public int? DisciplineId { get; set; }

        public string DisciplineName { get; set; }

        public int? OrganizationId { get; set; }

        public string OrganizationName { get; set; }

        public string OrganizationNameShort { get; set; }

        
    }
}