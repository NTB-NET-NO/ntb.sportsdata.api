// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Job.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The job.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace NTB.SportsData.Api.Domain.Classes
{
    /// <summary>
    ///     The job.
    /// </summary>
    public class Job
    {
        public int JobId { get; set; }

        /// <summary>
        ///     Gets or sets the job name.
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        ///     Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        ///     Gets or sets the tournament name.
        /// </summary>
        public string TournamentName { get; set; }

        /// <summary>
        ///     Gets or sets the gender id.
        /// </summary>
        public int GenderId { get; set; }

        /// <summary>
        ///     Gets or sets the min age.
        /// </summary>
        public int MinAge { get; set; }

        /// <summary>
        ///     Gets or sets the max age.
        /// </summary>
        public int MaxAge { get; set; }

        /// <summary>
        ///     Gets or sets the push.
        /// </summary>
        public int Push { get; set; }

        /// <summary>
        ///     Gets or sets the schedule property
        /// </summary>
        public bool Schedule { get; set; }

        /// <summary>
        ///     Gets or sets the customer id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        ///     Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        ///     Gets or sets the age category id.
        /// </summary>
        public int AgeCategoryId { get; set; }

        /// <summary>
        ///     Gets or sets the status for the job
        /// </summary>
        public int Status { get; set; }
    }
}