/**
 * This class shall be used in stead of the DistrictMunicipalityAndTournament Class. 
 * It shall contain all the information needed to create the job view including
 * Job class
 * List of selected Tournaments
 * List of Districts
 * List of selected Municipalities
 * Selected AgeCategory
 * 
 * In other words - and as stated above: all the information needed to render the job
 * 
 */ 
namespace NTB.SportsData.Api.Domain.Classes
{
    public class JobViewModel
    {
    }
}