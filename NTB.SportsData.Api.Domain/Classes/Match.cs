// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Match.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The matches.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System;
using System.Collections.Generic;

namespace NTB.SportsData.Api.Domain.Classes
{
    /// <summary>
    /// The matches.
    /// </summary>
    public class Match
    {
        /// <summary>
        /// Gets or sets the match id.
        /// </summary>
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public string SportName { get; set; }

        /// <summary>
        /// Gets or sets the home team.
        /// </summary>
        public string HomeTeam { get; set; }

        /// <summary>
        /// Gets or sets the away team.
        /// </summary>
        public string AwayTeam { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        public DateTime MatchDate { get; set; }

        /// <summary>
        /// Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        ///     Gets or sets the Tournament name
        /// </summary>
        public string TournamentName { get; set; }

        /// <summary>
        ///     Gets or sets the tournament age category id
        /// </summary>
        public int TournamentAgeCategoryId { get; set; }

        /// <summary>
        ///     Gets or sets the Away Team id
        /// </summary>
        public int AwayTeamId { get; set; }

        /// <summary>
        ///     Gets or sets the Away Team Goal
        /// </summary>
        public int AwayTeamGoal { get; set; }

        /// <summary>
        ///     Gets or sets the Home Team id
        /// </summary>
        public int HomeTeamId { get; set; }

        /// <summary>
        ///     Gets or sets the Home Team Goal
        /// </summary>
        public int HomeTeamGoal { get; set; }

        /// <summary>
        ///     Gets or sets the District Id
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        ///     Gets or sets the District Id
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        ///     Gets or sets the Match Start Time
        /// </summary>
        public string MatchStartTime { get; set; }

        /// <summary>
        ///     Gets or sets the User Id
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        ///     Gets or sets the Contact Persons for this match
        /// </summary>
        public List<ContactPerson> HomeTeamContactPersons { get; set; }

        /// <summary>
        ///     Gets or sets the away team contact persons for this match
        /// </summary>
        public List<ContactPerson> AwayTeamContactPersons { get; set; }

        /// <summary>
        ///     Gets or sets Comments about the match
        /// </summary>
        public string MatchComment { get; set; }

        /// <summary>
        ///     Gets or sets number of spectators
        /// </summary>
        public int Spectators { get; set; }

        /// <summary>
        ///     Gets or sets the name of the venue
        /// </summary>
        public string VenueName { get; set; }

        /// <summary>
        ///     Gets or sets the tournament round number
        /// </summary>
        public int TournamentRoundNumber { get; set; }

        /// <summary>
        ///     The short version of the organization
        /// </summary>
        public string OrganizationNameShort { get; set; }
    }
}