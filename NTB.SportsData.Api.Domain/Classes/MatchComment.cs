namespace NTB.SportsData.Api.Domain.Classes
{
    public class MatchComment
    {
        public int Id { get; set; }
        public string Comment { get; set; }
    }
}