namespace NTB.SportsData.Api.Domain.Classes
{
    public class Organization
    {
        public int OrganizationId { get; set; }

        public string OrganizationName { get; set; }

        public int SingleSport { get; set; }

        public int TeamSport { get; set; }

        public string Sport { get; set; }

        public int SportId { get; set; }
    }
}