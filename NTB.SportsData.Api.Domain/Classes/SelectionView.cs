// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DistrictsAndMunicipalities.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The districts and municipalities.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using NTB.SportsData.Api.Domain.Classes.XML;

namespace NTB.SportsData.Api.Domain.Classes
{
    /// <summary>
    /// The districts and municipalities.
    /// </summary>
    public class SelectionView
    {
        /// <summary>
        /// Gets or sets the municipalities.
        /// </summary>
        public List<Municipality> Municipalities { get; set; }

        /// <summary>
        /// Gets or sets the districts.
        /// </summary>
        public List<District> Districts { get; set; }

        /// <summary>
        /// Gets or sets the jobs tournament.
        /// </summary>
        public List<Tournament> Tournaments { get; set; }

        public DefineView DefineView { get; set; }
    }
}