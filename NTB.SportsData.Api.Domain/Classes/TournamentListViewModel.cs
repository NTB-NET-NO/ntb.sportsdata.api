﻿using System.Collections.Generic;
using NTB.SportsData.Api.Domain.Classes.XML;

namespace NTB.SportsData.Api.Domain.Classes
{
    public class TournamentListViewModel
    {
        public int SportId { get; set; }
        public List<AgeCategoryDefinition> AgeCategoryDefinitions { get; set; }
        public List<Tournament> Tournaments { get; set; }
    }
}
