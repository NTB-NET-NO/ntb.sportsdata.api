﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;

namespace NTB.SportsData.Api.Domain.Classes
{
    public class TournamentTable
    {
        public int TablePosition { get; set; }

        public int TeamId { get; set; }

        public int TournamentId { get; set; }
        
        public string TeamName { get; set; }

        public string JerseyColor { get; set; }

        public string ShortsColor { get; set; }

        public int TotalMatchesPlayed { get; set; }

        public int TotalMatchesWon { get; set; }

        public int TotalMatchesDraw { get; set; }

        public int TotalMatchesLost { get; set; }

        public int TotalGoalsScored { get; set; }

        public int TotalGoalsAgainst { get; set; }

        public int TotalGoalDifference { get; set; }

        public int TotalPoints { get; set; }

        public int HomeMatchesPlayed { get; set; }
                   
        public int HomeMatchesWon { get; set; }
                   
        public int HomeMatchesDraw { get; set; }
                   
        public int HomeMatchesLost { get; set; }
                   
        public int HomeGoalsScored { get; set; }
                   
        public int HomeGoalsAgainst { get; set; }
                   
        public int HomeGoalDifference { get; set; }

        public int HomePoints { get; set; }

        public int AwayMatchesPlayed { get; set; }
                   
        public int AwayMatchesWon { get; set; }
                   
        public int AwayMatchesDraw { get; set; }
                   
        public int AwayMatchesLost { get; set; }
                   
        public int AwayGoalsScored { get; set; }
                   
        public int AwayGoalsAgainst { get; set; }
                   
        public int AwayGoalDifference { get; set; }

        public int AwayPoints { get; set; }
    }
}
