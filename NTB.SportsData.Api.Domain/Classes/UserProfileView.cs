﻿using System.Collections.Generic;

namespace NTB.SportsData.Api.Domain.Classes
{
    public class UserProfileView
    {
        public List<UserProfile> UserProfiles { get; set; }
        
        public List<WorkRole> WorkRoles { get; set; }
    }
}
