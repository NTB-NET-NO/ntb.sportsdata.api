using System;

namespace NTB.SportsData.Api.Domain.Classes.XML
{
    public class Tournament
    {
       
        public int TournamentId { get; set; }

        public string TournamentName { get; set; }

        public string AgeCategoryDefinition { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int SportId { get; set; }

        public string SportName { get; set; }

        public int? AgeCategoryId { get; set; }

        public string AgeCategory { get; set; }

        public int SeasonId { get; set; }

        public string SeasonName { get; set; }

        public int? DisciplineId { get; set; }

        public string DisciplineName { get; set; }

        public int? OrganizationId { get; set; }

        public string OrganizationName { get; set; }

        public string OrganizationNameShort { get; set; }
    }
}