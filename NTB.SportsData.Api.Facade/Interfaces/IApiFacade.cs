﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Facade.Interfaces
{
    public interface IApiFacade
    {
        List<Domain.Classes.Team> GetTournamentTeams(int tournamentId);
        List<TournamentTable> GetTournamentStanding(Tournament tournament);
    }
}
