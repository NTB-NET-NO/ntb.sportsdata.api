using Glue;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.Facade.Mappers.NFF
{
    public class DistrictMapper : BaseMapper<District, Domain.Classes.District>
    {
        protected override void SetUpMapper(Mapping<District, Domain.Classes.District> mapper)
        {
            mapper.Relate(x => x.DistrictId, y => y.DistrictId);
            mapper.Relate(x => x.DistrictName, y => y.DistrictName);
        }
    }
}
