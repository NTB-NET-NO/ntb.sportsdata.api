﻿using Glue;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.Facade.Mappers.NFF
{
    public class MatchMapper : BaseMapper<Match, Domain.Classes.Match>
    {
        protected override void SetUpMapper(Mapping<Match, Domain.Classes.Match> mapper)
        {
            mapper.Relate(x => x.AwayTeamName, y => y.AwayTeam);
            mapper.Relate(x => x.AwayTeamId, y => y.AwayTeamId);
            mapper.Relate(x => x.MatchStartDate, y => y.MatchDate);
            mapper.Relate(x => x.HomeTeamName, y => y.HomeTeam);
            mapper.Relate(x => x.HomeTeamId, y => y.HomeTeamId);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentName, y => y.TournamentName);
            mapper.Relate(x => x.TournamentAgeCategoryId, y => y.TournamentAgeCategoryId);
        }
    }
}
