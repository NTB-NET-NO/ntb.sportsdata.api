using Glue;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.Facade.Mappers.NFF
{
    class MunicipalityMapper : BaseMapper<Municipality, Domain.Classes.Municipality>
    {
        protected override void SetUpMapper(Mapping<Municipality, Domain.Classes.Municipality> mapper)
        {
            mapper.Relate(x => x.Id, y=> y.MunicipalityId);
            mapper.Relate(x => x.Name, y => y.MuniciaplityName);
        }
    }
}