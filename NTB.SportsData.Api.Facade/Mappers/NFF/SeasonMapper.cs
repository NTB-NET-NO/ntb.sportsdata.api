using Glue;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.Facade.Mappers.NFF
{
    public class SeasonMapper : BaseMapper<Season, Domain.Classes.Season>
    {
        protected override void SetUpMapper(Mapping<Season, Domain.Classes.Season> mapper)
        {
            mapper.Relate(x => x.SeasonId, y=>y.SeasonId);
            mapper.Relate(x => x.SeasonNameLong, y => y.SeasonName);
            mapper.Relate(x => x.SeasonStartDate, y => y.SeasonStartDate);
            mapper.Relate(x => x.SeasonEndDate, y => y.SeasonEndDate);
            mapper.Relate(x => x.Ongoing, y=>y.SeasonActive);
        }
    }
}