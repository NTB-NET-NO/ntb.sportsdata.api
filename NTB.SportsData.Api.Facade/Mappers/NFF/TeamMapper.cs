using Glue;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.Facade.Mappers.NFF
{
    public class TeamMapper : BaseMapper<Team, Domain.Classes.Team>
    {
        protected override void SetUpMapper(Mapping<Team, Domain.Classes.Team> mapper)
        {
            mapper.Relate(x => x.TeamId, y=>y.TeamId);
            mapper.Relate(x => x.TeamName, y=>y.TeamName);
            mapper.Relate(x => x.TeamNameInTournament, y => y.TeamNameInTournament);
            mapper.Relate(x => x.AgeCategoryId, y => y.AgeCategoryId);
            mapper.Relate(x => x.ClubId, y => y.ClubId);
            mapper.Relate(x => x.GenderId, y => y.GenderId);
            
        }
    }
}