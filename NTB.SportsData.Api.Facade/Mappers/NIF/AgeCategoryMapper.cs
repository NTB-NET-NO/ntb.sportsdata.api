﻿using Glue;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Facade.Mappers.NIF
{
    public class AgeCategoryMapper : BaseMapper<ClassCode, AgeCategory>
    {
        protected override void SetUpMapper(Mapping<ClassCode, AgeCategory> mapper)
        {
            mapper.Relate(x => x.ClassId, y => y.CategoryId);
            mapper.Relate(x => x.Name, y => y.CategoryName);
            mapper.Relate(x => x.ToAge, y => y.MaxAge);
            mapper.Relate(x => x.FromAge, y => y.MinAge);
            mapper.Relate(x => x.ActivityId, y => y.SportId);
        }
    }
}
