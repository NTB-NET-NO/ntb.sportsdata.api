﻿using Glue;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Facade.Mappers.NIF
{
    public class DistrictMapper : BaseMapper<Region, District>
    {
        protected override void SetUpMapper(Mapping<Region, District> mapper)
        {
            mapper.Relate(x => x.RegionId, y => y.DistrictId);
            mapper.Relate(x => x.RegionName, y => y.DistrictName);
        }
    }
}
