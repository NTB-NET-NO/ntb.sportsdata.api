using Glue;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Facade.Mappers.NIF
{
    public class FederationMapper : BaseMapper<Federation, Domain.Classes.Federation>
    {
        // 71:132:Rinkbandy;72:133:Innebandy;73:134:Hockey;151:131:Bandy
        protected override void SetUpMapper(Mapping<Federation, Domain.Classes.Federation> mapper)
        {
            mapper.Relate(x => x.OrgId, y => y.FederationId);
            mapper.Relate(x => x.OrgName, y => y.FederationName);
            mapper.Relate(x => x.BranchCode, y => y.BranchCode);
            mapper.Relate(x => x.BranchId, y => y.BranchId);
            mapper.Relate(x => x.BranchName, y => y.BranchName);
            //mapper.Relate(x => x.Disciplines.Replace("&amp;", "&").Split(';')
            //                              .Select(s => s.Split(':'))
            //                              .Select(a => new FederationDiscipline
            //                                  {
            //                                      ActivityCode = Convert.ToInt32(a[0].ToString()),
            //                                      ActivityId = Convert.ToInt32(a[1].ToString()),
            //                                      ActivityName = a[2]
            //                                  }).ToList(), y => y.FederationDisciplines);
            mapper.Relate(x => x.HasIndividualResults, y => y.HasIndividualResults);
            mapper.Relate(x => x.HasTeamResults, y => y.HasTeamResults);
        }
    }
}