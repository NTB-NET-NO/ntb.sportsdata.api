﻿using Glue;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Facade.Mappers.NIF
{
    public class MunicipalityMapper : BaseMapper<Region, Municipality>
    {
        protected override void SetUpMapper(Mapping<Region, Municipality> mapper)
        {
            mapper.Relate(x=>x.RegionId, y=>y.MunicipalityId);
            mapper.Relate(x=>x.RegionName, y=>y.MuniciaplityName);
        }
    }
}
