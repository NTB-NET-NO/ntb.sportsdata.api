using Glue;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Mappers.Base;
using FederationDiscipline = NTB.SportsData.Services.NIF.NIFProdService.FederationDiscipline;

namespace NTB.SportsData.Facade.Mappers.NIF
{
    public class SportMapper : BaseMapper<FederationDiscipline, Sport>
    {
        protected override void SetUpMapper(Mapping<FederationDiscipline, Sport> mapper)
        {
            mapper.Relate(x => x.ActivityName, y=>y.Name);
            mapper.Relate(x => x.ActivityId, y => y.Id);
        }
    }
}


