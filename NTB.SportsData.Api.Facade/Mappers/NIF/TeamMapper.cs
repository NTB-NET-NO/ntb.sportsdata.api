using Glue;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Facade.Mappers.NIF
{
    public class TeamMapper : BaseMapper<TournamentMatchTeam, Domain.Classes.Team>
    {
        protected override void SetUpMapper(Mapping<TournamentMatchTeam, Domain.Classes.Team> mapper)
        {
            mapper.Relate(x => x.TeamId, y=>y.TeamId);
            mapper.Relate(x => x.Team, y=>y.TeamName);
        }
    }
}