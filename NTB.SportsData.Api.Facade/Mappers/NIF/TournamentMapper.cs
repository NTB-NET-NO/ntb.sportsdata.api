using Glue;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Facade.Mappers.NIF
{
    public class TournamentMapper : BaseMapper<Tournament, Domain.Classes.Tournament>
    {
        protected override void SetUpMapper(Mapping<Tournament, Domain.Classes.Tournament> mapper)
        {
            mapper.Relate(x => x.ClassCodeId, y => y.AgeCategoryId);
            mapper.Relate(x => x.Division, y => y.Division);
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.ActivityId, y => y.SportId);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentName, y => y.TournamentName);
            mapper.Relate(x => x.TournamentNo, y=>y.TournamentNumber);
        }
    }
}