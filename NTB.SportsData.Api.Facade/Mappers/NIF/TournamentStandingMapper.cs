﻿using Glue;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Facade.Mappers.NIF
{
    public class TournamentStandingMapper : BaseMapper<TournamentTable, Domain.Classes.TournamentTable>
    {
        protected override void SetUpMapper(Mapping<TournamentTable, Domain.Classes.TournamentTable> mapper)
        {
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.OrgElementId, y => y.TeamId);
            mapper.Relate(x => x.TeamName, y => y.TeamName);
            mapper.Relate(x => x.OrderIdentity, y => y.TablePosition);
            mapper.Relate(x => x.NoMatches, y => y.TotalMatchesPlayed);
            mapper.Relate(x => x.NoWins, y => y.TotalMatchesWon);
            mapper.Relate(x => x.NoDraws, y => y.TotalMatchesDraw);
            mapper.Relate(x => x.NoLosses, y => y.TotalMatchesLost);
            mapper.Relate(x => x.NoGoals, y => y.TotalGoalsScored);
            mapper.Relate(x => x.NoAgainst, y => y.TotalGoalsAgainst);
            mapper.Relate(x => x.NoDiff, y => y.TotalGoalDifference);
            mapper.Relate(x => x.NoAwayMatches,     y => y.AwayMatchesPlayed);
            mapper.Relate(x => x.NoAwayWins,        y => y.AwayMatchesWon);
            mapper.Relate(x => x.NoAwayDraws,       y => y.AwayMatchesDraw);
            mapper.Relate(x => x.NoAwayLosses,       y => y.AwayMatchesLost);
            mapper.Relate(x => x.NoAwayGoals,       y => y.AwayGoalsScored);
            mapper.Relate(x => x.NoAwayAgainst,      y => y.AwayGoalsAgainst);
            mapper.Relate(x => x.NoHomeMatches,     y => y.HomeMatchesPlayed);
            mapper.Relate(x => x.NoHomeWins,        y => y.HomeMatchesWon);
            mapper.Relate(x => x.NoHomeDraws,       y => y.HomeMatchesDraw);
            mapper.Relate(x => x.NoHomeLosses,       y => y.HomeMatchesLost);
            mapper.Relate(x => x.NoHomeGoals,       y => y.HomeGoalsScored);
            mapper.Relate(x => x.NoHomeAgainst,      y => y.HomeGoalsAgainst);
        }
    }
}
