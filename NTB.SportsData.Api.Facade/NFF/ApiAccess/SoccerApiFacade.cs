﻿using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Interfaces;
using NTB.SportsData.Facade.Mappers.NFF;
using NTB.SportsData.Services.NFF.Interfaces;
using NTB.SportsData.Services.NFF.Repositories;

namespace NTB.SportsData.Facade.NFF.ApiAccess
{
    public class SoccerApiFacade : IApiFacade
    {
        private readonly ITeamDataMapper _teamDataMapper;
        private readonly ITournamentDataMapper _tournamentDataMapper;

        public SoccerApiFacade()
        {
            _teamDataMapper = new TeamRepository();
            _tournamentDataMapper = new TournamentRepository();
        }

        public List<Team> GetTournamentTeams(int tournamentId)
        {
            var result = _teamDataMapper.GetTournamentTeams(tournamentId);
            
            var mapper = new TeamMapper();

            return result.Select(row => mapper.Map(row, new Team())).ToList();

        }

        public List<Domain.Classes.TournamentTable> GetTournamentStanding(Tournament tournament)
        {
            var result = _tournamentDataMapper.GetTournamentStanding(tournament.TournamentId);

            var mapper = new TournamentStandingMapper();

            return result.Select(row => mapper.Map(row, new Domain.Classes.TournamentTable())).ToList();
            
        }
    }
}
