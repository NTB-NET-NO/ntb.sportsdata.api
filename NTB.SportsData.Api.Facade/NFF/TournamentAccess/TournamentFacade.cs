﻿using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Mappers.NFF;
using NTB.SportsData.Facade.NFF.TournamentInterface;
using NTB.SportsData.Services.NFF.Interfaces;
using NTB.SportsData.Services.NFF.Repositories;

namespace NTB.SportsData.Facade.NFF.TournamentAccess
{

    public class TournamentFacade : ITournamentFacade
    {
        private readonly ITournamentDataMapper _tournamentDataMapper;
        private readonly IDistrictDataMapper _districtDataMapper;
        private readonly IMunicipalityDataMapper _municipalityDataMapper;
        private readonly ISeasonDataMapper _seasonDataMapper;
        private readonly IAgeCategoryDataMapper _ageCategoryDataMapper;
        private readonly IMatchDataMapper _matchDataMapper;

        // Using the repository interface
        private readonly IRepository<Municipality> _municipalityRepository;
        private readonly ITeamDataMapper _teamDataMapper;

        public TournamentFacade()
        {
            _tournamentDataMapper = new TournamentRepository();
            _districtDataMapper = new DistrictRepository();
            _municipalityDataMapper = new MunicipalityRepository();
            _seasonDataMapper = new SeasonRepository();
            _ageCategoryDataMapper = new AgeCategoryRepository();
            _teamDataMapper = new TeamRepository();
            _matchDataMapper = new MatchRepository();


            // Repository code
            
        }

        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId)
        {
            var result = _tournamentDataMapper.GetTournamentByMunicipalities(municipalities, seasonId);

            var mapper = new TournamentMapper();

            return result.Select(row => mapper.Map(row, new Tournament())).ToList();
        }


        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            var result = _tournamentDataMapper.GetTournamentsByDistrict(districtId, seasonId);

            TournamentMapper mapper = new TournamentMapper();

            var tournaments = result.Select(row => mapper.Map(row, new Tournament())).ToList();

            List<Tournament> t2 = new List<Tournament>();
            foreach (Tournament tournament in tournaments)
            {
                Tournament t1 = tournament;
                t1.SportId = 16;

                t2.Add(t1);
            }

            return t2;
        }

        public List<Domain.Classes.Tournament> GetTournamentsByTeam(int teamId, int seasonId)
        {
            var result = _tournamentDataMapper.GetTournamentsByTeam(teamId, seasonId);

            TournamentMapper mapper = new TournamentMapper();

            return result.Select(row => mapper.Map(row, new Domain.Classes.Tournament())).ToList();
        }


        public List<Domain.Classes.District> GetDistricts()
        {
            var districts = _districtDataMapper.GetDistricts();

            var districtMapper = new DistrictMapper();

            return districts.Select(row => districtMapper.Map(row, new Domain.Classes.District())).ToList();
        }

        public Domain.Classes.District GetDistrict(int id)
        {
            var result = _districtDataMapper.GetDistrict(id);

            var districtMapper = new DistrictMapper();

            return districtMapper.Map(result, new Domain.Classes.District());
        }

        public List<Domain.Classes.Municipality> GetMunicipalities()
        {
            var result = _municipalityDataMapper.GetAll();

            var municipalityMapper = new MunicipalityMapper();

            return result.Select(row => municipalityMapper.Map(row, new Domain.Classes.Municipality())).ToList();
        }

        public List<Domain.Classes.Municipality> GetMunicipalitiesbyDistrict(int districtId)
        {
            var results = _municipalityDataMapper.GetMunicipalitiesbyDistrict(districtId);

            var municipalityMapper = new MunicipalityMapper();

            return results.Select(row => municipalityMapper.Map(row, new Domain.Classes.Municipality())).ToList();
        }

        public List<Domain.Classes.Tournament> GetClubsByTournament(int tournamentId, int seasonId)
        {
            throw new NotImplementedException();
        }

        public List<Domain.Classes.Season> GetSeasons()
        {
            SeasonMapper mapper = new SeasonMapper();

            var result = _seasonDataMapper.GetSeasons();

            return result.Select(row => mapper.Map(row, new Domain.Classes.Season())).ToList();
        }

        public Domain.Classes.Season GetSeason(int id)
        {
            var result = _seasonDataMapper.GetSeason(id);

            var mapper = new SeasonMapper();

            return mapper.Map(result, new Domain.Classes.Season());
        }

        public Domain.Classes.Season GetOngoingSeason()
        {
            var result = _seasonDataMapper.GetOngoingSeason();

            var mapper = new SeasonMapper();

            return mapper.Map(result, new Domain.Classes.Season());
        }


        public List<AgeCategory> GetAgeCategoriesTournament()
        {
            var mapper = new AgeCategoryMapper();
            var result = _ageCategoryDataMapper.GetAgeCategoriesTournament();

            return result.Select(row => mapper.Map(row, new AgeCategory())).ToList();
        }

        public List<Domain.Classes.Match> GetMatchesByTournament(int tournamentId, bool includeSquad,
                                                                 bool includeReferees, bool includeResults,
                                                                 bool includeEvents)
        {
            var mapper = new MatchMapper();

            var result = _matchDataMapper.GetMatchesByTournament(tournamentId, includeSquad, includeReferees,
                                                                 includeResults, includeEvents);
            return result.Select(row => mapper.Map(row, new Domain.Classes.Match())).ToList();

        }

        public AgeCategory GetAgeCategoryTournament()
        {
            throw new NotImplementedException();
        }

        public List<Domain.Classes.Team> GetTeamsByMunicipality(int municipalityId, int seasonId)
        {
            throw new NotImplementedException();
        }

        public Tournament GetTournament(int tournamentId)
        {
            var result = _tournamentDataMapper.GetTournament(tournamentId);

            var mapper = new TournamentMapper();

            return mapper.Map(result, new Tournament());
        }

        public List<Domain.Classes.Match> GetTodaysMatches(DateTime matchDate)
        {
            var result = _matchDataMapper.GetTodaysMatches(matchDate);

            var mapper = new MatchMapper();

            return result.Select(row => mapper.Map(row, new Match())).ToList();
        }

        public List<ContactPerson> GetTeamContacts(int teamId)
        {
            var result = _teamDataMapper.GetTeamPersons(teamId);

            var mapper = new ContactPersonMapper();

            return result.Select(row => mapper.Map(row, new ContactPerson())).ToList();
        }
    }
}