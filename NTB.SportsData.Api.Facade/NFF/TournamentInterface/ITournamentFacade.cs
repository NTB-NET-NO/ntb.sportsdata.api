using System;
using System.Collections.Generic;

namespace NTB.SportsData.Facade.NFF.TournamentInterface
{
    interface ITournamentFacade
    {
        // Returns list
        List<Domain.Classes.Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId);
        List<Domain.Classes.Tournament> GetTournamentsByDistrict(int districtId, int seasonId);
        List<Domain.Classes.Tournament> GetClubsByTournament(int tournamentId, int seasonId);
        List<Domain.Classes.Tournament> GetTournamentsByTeam(int teamId, int seasonId);
        Domain.Classes.Tournament GetTournament(int tournamentId);

        List<Domain.Classes.District> GetDistricts();
        Domain.Classes.District GetDistrict(int id);

        List<Domain.Classes.Municipality> GetMunicipalities();
        List<Domain.Classes.Municipality> GetMunicipalitiesbyDistrict(int districtId);

        List<Domain.Classes.Season> GetSeasons();
        Domain.Classes.Season GetSeason(int id);
        Domain.Classes.Season GetOngoingSeason();

        List<Domain.Classes.AgeCategory> GetAgeCategoriesTournament();
        Domain.Classes.AgeCategory GetAgeCategoryTournament();

        List<Domain.Classes.Team> GetTeamsByMunicipality(int municipalityId, int seasonId);

        List<Domain.Classes.Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees,
                                                          bool includeResults, bool includeEvents);

        List<Domain.Classes.Match> GetTodaysMatches(DateTime matchDate);

        List<Domain.Classes.ContactPerson> GetTeamContacts(int teamId);
    }
}
