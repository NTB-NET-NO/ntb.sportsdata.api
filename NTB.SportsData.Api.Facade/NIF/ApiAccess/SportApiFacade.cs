﻿using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Interfaces;
using NTB.SportsData.Facade.Mappers.NIF;
using NTB.SportsData.Services.NIF.Interfaces;
using NTB.SportsData.Services.NIF.Repositories;

namespace NTB.SportsData.Facade.NIF.ApiAccess
{
    public class SportApiFacade : IApiFacade
    {
        private readonly ITeamDataMapper _teamDataMapper;
        private readonly ITournamentDataMapper _tournamentDataMapper;

        public SportApiFacade()
        {
            _teamDataMapper = new TeamRepository();
            _tournamentDataMapper =  new TournamentRepository();
        }

        public List<Team> GetTournamentTeams(int tournamentId)
        {
            var result = _teamDataMapper.GetTournamentTeams(tournamentId);

            var mapper = new TeamMapper();

            return result.Select(row => mapper.Map(row, new Team())).ToList();
            throw new NotImplementedException();
        }

        public List<TournamentTable> GetTournamentStanding(Tournament tournament)
        {
            var result = _tournamentDataMapper.GetTournamentStanding(tournament.TournamentId);

            var mapper = new TournamentStandingMapper();

            return result.Select(row => mapper.Map(row, new TournamentTable())).ToList();
        }
    }
}
