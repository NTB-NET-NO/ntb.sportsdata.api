﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Facade.NIF.SportInterface
{
    interface ISportFacade
    {
        List<FederationDiscipline> GetSports();
        FederationDiscipline GetSport(int id);

        List<Federation> GetFederations();
        Federation GetFederation(int id);
        Federation GetFederationByOrgId(int orgId);

        List<FederationDiscipline> GetFederationDisciplines(int orgId);
        FederationDiscipline GetFederationDiscipline(int id);

        List<District> GetDistricts();
        List<Municipality> GetMunicipalities();

        List<AgeCategory> GetAgeCategories(int orgId);

        List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId, int orgId);

        List<AgeCategory> GetClassCodes(int orgId);

        List<Season> GetFederationSeasons(int orgId);

        List<Match> GetMatchesByTournamentIdAndSportId(int tournamentId, int sportId);

    }
}
