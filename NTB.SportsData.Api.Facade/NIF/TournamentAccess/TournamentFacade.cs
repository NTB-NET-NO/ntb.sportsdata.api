﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.NIF.TournamentInterface;

namespace NTB.SportsData.Facade.NIF.TournamentAccess
{
    public class TournamentFacade : ITournamentFacade
    {
        public TournamentFacade()
        {
        }

        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId)
        {
            throw new System.NotImplementedException();
        }

        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            throw new System.NotImplementedException();
        }

        public List<Tournament> GetTournamentsByTeam(int teamId, int seasonId)
        {
            throw new System.NotImplementedException();
        }

        public List<Match> GetTodaysMatches(DateTime matchDate)
        {
            return new List<Match>();
        }
    }
}
