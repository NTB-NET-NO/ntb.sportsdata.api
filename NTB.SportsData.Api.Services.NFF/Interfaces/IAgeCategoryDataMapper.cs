﻿using System.Collections.Generic;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.Services.NFF.Interfaces
{
    public interface IAgeCategoryDataMapper
    {
        List<AgeCategoryTeam> GetAgeCategoriesTeam();
        List<AgeCategoryTournament> GetAgeCategoriesTournament();
    }
}
