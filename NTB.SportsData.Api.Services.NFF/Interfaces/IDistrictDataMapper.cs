﻿using System.Collections.Generic;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.Services.NFF.Interfaces
{
    public interface IDistrictDataMapper
    {
        List<District> GetDistricts();
        District GetDistrict(int districtId);
    }
}
