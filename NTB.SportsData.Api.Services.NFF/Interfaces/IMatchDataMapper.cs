﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.Services.NFF.Interfaces
{
    public interface IMatchDataMapper
    {
        List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees,
                                           bool includeResults, bool includeEvents);

        List<Match> GetTodaysMatches(DateTime matchDate);
    }
}
