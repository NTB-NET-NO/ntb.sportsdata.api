﻿using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.Services.NFF.Interfaces
{
    public interface IMunicipalityDataMapper
    {
        List<Municipality> GetMunicipalitiesbyDistrict(int districtId);
        IQueryable<Municipality> GetAll();
    }
}
