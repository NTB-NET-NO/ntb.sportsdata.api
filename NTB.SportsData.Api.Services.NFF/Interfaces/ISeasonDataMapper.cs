﻿using System.Collections.Generic;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.Services.NFF.Interfaces
{
    public interface ISeasonDataMapper
    {
        List<Season> GetSeasons();
        Season GetSeason(int id);
        Season GetOngoingSeason();
    }
}
