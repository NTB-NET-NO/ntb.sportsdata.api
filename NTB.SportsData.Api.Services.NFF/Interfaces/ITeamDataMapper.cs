﻿using System.Collections.Generic;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.Services.NFF.Interfaces
{
    public interface ITeamDataMapper
    {
        List<Team> GetTournamentTeams(int tournamentId);
        List<Team> GetTeamsByMunicipality(int municipalityId);
        List<OrganizationPerson> GetTeamPersons(int teamId);
    }
}
