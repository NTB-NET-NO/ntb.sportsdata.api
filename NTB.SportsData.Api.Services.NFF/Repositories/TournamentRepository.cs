using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Services.NFF.Interfaces;
using NTB.SportsData.Services.NFF.NFFProdService;
using NTB.SportsData.Services.NFF.Utilities;

namespace NTB.SportsData.Services.NFF.Repositories
{
    public class TournamentRepository : IRepository<Tournament>, IDisposable, ITournamentDataMapper
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof (TournamentRepository));

        private MetaServiceClient _metaServiceClient = new MetaServiceClient();
        private OrganizationServiceClient _organizationServiceClient = new OrganizationServiceClient();
        private TournamentServiceClient _tournamentServiceClient = new TournamentServiceClient();

        public int AgeCategoryId { get; set; }
        public TournamentRepository()
        {
            if (_tournamentServiceClient.ClientCredentials != null)
            {
                _tournamentServiceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServicesUsername"];
                _tournamentServiceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicesPassword"];
            }
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int InsertOne(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Tournament> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Tournament> GetAll()
        {
            throw new NotImplementedException();
        }


        public Tournament Get(int id)
        {
            CheckServiceState(_tournamentServiceClient);

            return _tournamentServiceClient.GetTournament(id);
        }

        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            // CheckServiceState(_tournamentServiceClient);
            try
            {
                return
                    new List<Tournament>(_tournamentServiceClient.GetTournamentsByDistrict(districtId, seasonId, null));
            }
            catch (Exception exception)
            {
                Logger.Info("DistrictId: " + districtId + ", SeasonId: " + seasonId);
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace); 

                return new List<Tournament>();
            }
        }

        public List<Tournament> GetTournamentsByTeam(int teamId, int seasonId)
        {
            CheckServiceState(_tournamentServiceClient);

            return new List<Tournament>(_tournamentServiceClient.GetTournamentsByTeam(teamId, seasonId, null));
        }

        public List<Tournament> GetClubsByTournament(int tournamentId, int seasonId)
        {
            CheckServiceState(_organizationServiceClient);
            CheckServiceState(_tournamentServiceClient);
            CheckServiceState(_metaServiceClient);

            Club[] clubs = _organizationServiceClient.GetClubsByTournament(tournamentId, null);

            foreach (Club club in clubs)
            {
                Match[] clubMatches = _tournamentServiceClient.GetMatchesByClub(club.ClubId, seasonId, true, false,
                                                                                false, true,
                                                                                null);
                foreach (Match clubMatch in clubMatches)
                {
                    foreach (MatchEvent matchEvent in clubMatch.MatchEventList)
                    {
                        //if (matchEvent.MatchEventTypeId == (int) EventType.Advarsel)
                        //{
                        //    // We are doing something
                        //}
                    }
                }
            }
            return new List<Tournament>();
        }

        public List<AgeCategoryTournament> GetAgeCategoriesTournament()
        {
            CheckServiceState(_organizationServiceClient);
            CheckServiceState(_tournamentServiceClient);
            CheckServiceState(_metaServiceClient);

            return _metaServiceClient.GetAgeCategoriesTournament().ToList();
        }

        public List<Team> GetTeamsByMunicipality(int municipalityId)
        {
            CheckServiceState(_organizationServiceClient);
            CheckServiceState(_tournamentServiceClient);
            CheckServiceState(_metaServiceClient);

            return _organizationServiceClient.GetTeamsByMunicipality(municipalityId, null).ToList();
        }

        public Tournament GetTournament(int tournamentId)
        {
            CheckServiceState(_tournamentServiceClient);
            return _tournamentServiceClient.GetTournament(tournamentId);
        }

        public List<TournamentTableTeam> GetTournamentStanding(int tournamentId)
        {
            if (_tournamentServiceClient.ClientCredentials != null)
            {
                _tournamentServiceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServicesUsername"];
                _tournamentServiceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicesPassword"];
            }

            var tournamentStandings = new List<TournamentTableTeam>();

            try
            {
                var response = _tournamentServiceClient.GetTournamentStanding(tournamentId, null).ToList();

                tournamentStandings.AddRange(response);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                throw new Exception(exception.Message);
            }

            return tournamentStandings;
        }

        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId)
        {
            if (_organizationServiceClient.ClientCredentials != null)
            {
                _organizationServiceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServicesUsername"];
                _organizationServiceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicesPassword"];
            }

            if (_tournamentServiceClient.ClientCredentials != null)
            {
                _tournamentServiceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServicesUsername"];
                _tournamentServiceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicesPassword"];
            }

            var teamList = new List<Team>();

            // First we get the teams in the municipalities.
            foreach (string stringMunicipalityId in municipalities)
            {
                int municipalityId = Convert.ToInt32(stringMunicipalityId);

                // Getting the teams from the municipalities and find out in which tournaments they are participating in...

                // TODO: fix so that it gets the list from the service and then check if this is in the database for this customer.
                // If it is for the customer, we shall mark it as selected.
                if (municipalityId <= 0 || municipalityId == 500)
                {
                    continue;
                }

                try
                {
                    // I also need to store the municipalities in the database
                    // Insert into municipality has been moved to the controller, as we do everything there
                    // Consider - we are looping there, and we are looping here. What is best practice?
                    //            could this be a speed issue?

                    // municipalitiesDataAccess.InsertMunicipality(municipalityId, jobId);

                    Team[] teams = _organizationServiceClient.GetTeamsByMunicipality(municipalityId, null);

                    teamList.AddRange(teams);
                }
                catch (Exception exception)
                {
                    // throw new Exception(exception.Message);
                    Logger.Error(exception);
                }
            }

            List<Team> distinctTeams = new List<Team>();
            var teamComparer = new TeamComparer();
            foreach (Team team in teamList.Where(team => !distinctTeams.Contains(team, teamComparer)))
            {
                distinctTeams.Add(team);
            }

            // Clearing the list of teams
            teamList.Clear();
            
            var listTournaments = new List<Tournament>();
            foreach (Team team in distinctTeams)
            {
                try
                {
                    Tournament[] tournaments =
                        _tournamentServiceClient.GetTournamentsByTeam(team.TeamId, seasonId, null);

                    listTournaments.AddRange(tournaments);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    // we just log this as it is OK not to receive tournaments 

                    // throw new Exception(exception.Message);
                }
            }

            // First filtration. Checking that we have distinct list of tournaments
            var tournamentComparer = new TournamentComparer();
            List<Tournament> distinctTournaments = new List<Tournament>();
            foreach (Tournament tournament in listTournaments.Where(tournament => !distinctTournaments.Contains(tournament, tournamentComparer)))
            {
                distinctTournaments.Add(tournament);
            }

            if (!distinctTournaments.Any())
            {
                return null;
            }


            List<Tournament> filteredTournaments;

            if (AgeCategoryId == 2)
            {
                filteredTournaments = (from tournament in distinctTournaments
                                       where
                                           (tournament.TournamentAgeCategory == 18)
                                           || (tournament.TournamentAgeCategory == 21)
                                       orderby tournament.Division
                                       select tournament).ToList();
            }
            else
            {
                filteredTournaments = (from tournament in distinctTournaments
                                       where
                                           !(tournament.TournamentAgeCategory == 18
                                             || tournament.TournamentAgeCategory == 21)
                                       orderby tournament.Division
                                       select tournament).ToList();
            }

            return filteredTournaments;
        }

        private void CheckServiceState(object service)
        {
            if (service.GetType() == new TournamentServiceClient().GetType())
            {
                _ResetTournamentServiceClient((TournamentServiceClient) service);
            }
            else if (service.GetType() == new OrganizationServiceClient().GetType())
            {
                _ResetOrganizationServiceClient((OrganizationServiceClient) service);
            }
            else if (service.GetType() == new MetaServiceClient().GetType())
            {
                _ResetMetaServiceClient((MetaServiceClient) service);
            }
        }

        private void _ResetTournamentServiceClient(TournamentServiceClient client)
        {
            if (client.ClientCredentials == null) return;

            if (client.State == CommunicationState.Closed)
            {
                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                client.Open();

                return;
            }

            if (client.State == CommunicationState.Faulted)
            {
                client.Close();

                client = null;

                client = new TournamentServiceClient();
                if (client.ClientCredentials == null) return;
                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                client.Open();

                return;

            }

            
            // Setting username and password again
            if (client.ClientCredentials.UserName.UserName == string.Empty)
            {
                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
            }

            if (client.ClientCredentials.UserName.Password == string.Empty)
            {
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
            }

        }

        private void _ResetMetaServiceClient(MetaServiceClient client)
        {
            client.Close();

            client = null;

            client = new MetaServiceClient();
            

            client.Open();
            if (client.ClientCredentials == null) return;

            // Setting username and password again
            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
        }

        private void _ResetOrganizationServiceClient(OrganizationServiceClient client)
        {
            client.Close();
            client = null;

            client = new OrganizationServiceClient();
            if (client.ClientCredentials == null) return;

            // Setting username and password again
            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
        }
    }
}