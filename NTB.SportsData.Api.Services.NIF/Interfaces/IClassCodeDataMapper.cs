﻿using System.Collections.Generic;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Services.NIF.Interfaces
{
    public interface IClassCodeDataMapper
    {
        List<ClassCode> GetClassCodes(int orgId);
    }
}
