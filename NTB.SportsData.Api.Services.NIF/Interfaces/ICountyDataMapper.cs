﻿using System.Collections.Generic;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Services.NIF.Interfaces
{
    public interface ICountyDataMapper
    {
        List<Region> GetCounties();
    }
}
