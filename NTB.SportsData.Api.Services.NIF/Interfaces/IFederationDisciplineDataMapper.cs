﻿using System.Collections.Generic;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Services.NIF.Interfaces
{
    public interface IFederationDisciplineDataMapper
    {
        List<FederationDiscipline> GetFederationDisciplines(int orgId);
        FederationDiscipline GetFederationDiscipline(int id);
    }
}
