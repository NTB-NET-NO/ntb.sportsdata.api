﻿using NTB.SportsData.Services.NIF.NIFProdService;
namespace NTB.SportsData.Services.NIF.Interfaces
{
    public interface IPersonDataMapper
    {
        Person GetPersonById(int id);
    }
}
