﻿using System.Collections.Generic;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Services.NIF.Interfaces
{
    public interface ISeasonDataMapper
    {
        List<Season> GetFederationSeasonByOrgId(int orgId);
    }
}
