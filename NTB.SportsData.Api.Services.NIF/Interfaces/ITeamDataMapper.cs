﻿using System.Collections.Generic;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Services.NIF.Interfaces
{
    public interface ITeamDataMapper
    {
        List<Person> GetTeamPersons(int teamId);
        List<TournamentMatchTeam> GetTournamentTeams(int tournamentId);
    }
}
