﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using NTB.SportsData.Services.NIF.Interfaces;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Services.NIF.Repositories
{
    public class MatchRepository : IRepository<TournamentMatch>, IDisposable, IMatchDataMapper 
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentRepository));

        public int InsertOne(TournamentMatch domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<TournamentMatch> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(TournamentMatch domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(TournamentMatch domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<TournamentMatch> GetAll()
        {
            throw new NotImplementedException();
        }

        public TournamentMatch Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<TournamentMatchExtended> GetMatchesByTournamentIdAndSportId(int tournamentId, int sportId)
        {
            TournamentServiceClient tournamentServiceClient = new TournamentServiceClient();

            if (tournamentServiceClient.ClientCredentials == null)
            {
                return new List<TournamentMatchExtended>();
            }

            tournamentServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"]; 
            tournamentServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            // Creating the database fetch request
            var fromDate = DateTime.Today.Date;
            var toDate = DateTime.Today.Date;

            var request = new TournamentMatchRequest
                {
                    TournamentId = tournamentId,
                    FromDate = fromDate,
                    ToDate = toDate
                };
            var response = new TournamentMatchesResponse(); 

            try
            {
                 response = tournamentServiceClient.GetTournamentMatches(request);
            }
            catch (Exception exception)
            {
               Logger.Error(exception); 
               throw;
            }

            return !response.Success ? new List<TournamentMatchExtended>() : response.TournamentMatch.ToList();
        }

        public List<TournamentMatch> GetTodaysMatches(DateTime matchDate, int seasonId)
        {
            TournamentServiceClient tournamentServiceClient = new TournamentServiceClient();

            if (tournamentServiceClient.ClientCredentials == null)
            {
                return new List<TournamentMatch>();
            }

            tournamentServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            tournamentServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            var seasonrequest = new SeasonIdRequest(seasonId);
            var tournamentResponse = tournamentServiceClient.GetSeasonTournaments(seasonrequest);

            if (!tournamentResponse.Success)
            {
                return null;
            }

            var tournaments = tournamentResponse.Tournaments;


            var request = new TournamentMatchRequest
                {
                    FromDate = matchDate,
                    ToDate = matchDate
                };
            var matchesResponse = tournamentServiceClient.GetTournamentMatches(request);

            if (matchesResponse.Success)
            {
                
            }
            return new List<TournamentMatch>();
        }
    }
}
