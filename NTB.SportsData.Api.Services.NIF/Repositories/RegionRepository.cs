﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsData.Services.NIF.Interfaces;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Services.NIF.Repositories
{
    public class RegionRepository : IRepository<Region>, IDisposable, IRegionDataMapper

    {
        /// <summary>
        /// The tournament service client.
        /// </summary>
        private readonly RegionService1Client _serviceClient = new RegionService1Client();

        public RegionRepository()
        {
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
                _serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];
            }
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Region> GetRegions()
        {
            var request = new EmptyRequest5();
            var response = _serviceClient.GetMuncipalities(request);

            return response.Success ? response.Regions.ToList() : new List<Region>();
        }

        public int InsertOne(Region domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Region> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Region domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Region domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Region> GetAll()
        {
            throw new NotImplementedException();
        }

        public Region Get(int id)
        {
            throw new NotImplementedException();
        }
    }
}
