﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsData.Services.NIF.Interfaces;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Services.NIF.Repositories
{
    public class TeamRepository : IRepository<MatchTeam>, IDisposable, ITeamDataMapper
    {
        private readonly TournamentServiceClient _serviceClient = new TournamentServiceClient();

        public TeamRepository()
        {
            // Setting up the OrganizationClient
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                _serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
            }
        }

        public int InsertOne(MatchTeam domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<MatchTeam> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(MatchTeam domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(MatchTeam domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<MatchTeam> GetAll()
        {
            throw new NotImplementedException();
        }

        public MatchTeam Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Person> GetTeamPersons(int teamId)
        {
            throw new NotImplementedException();
        }

        public List<TournamentMatchTeam> GetTournamentTeams(int tournamentId)
        {
            var request = new TournamentIdRequest
            {
                TournamentId = tournamentId
            };

            var response = _serviceClient.GetTournamentTeams(request);

            if (response.Success)
            {
                return response.TournamentMatchTeam.ToList();
            }

            return null;
        }
    }
}
