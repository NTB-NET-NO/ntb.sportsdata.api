﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;

namespace NTB.SportsData.Api.Areas.HelpPage.Interfaces
{
    /// <summary>
    ///     An interface to create special stuff
    /// </summary>
    public interface ICodeDocumentationProvider
    {
        string GetCodeDocumentation(HttpActionDescriptor actionDescriptor, string type);
    }
}
