﻿using System;
using NTB.SportsData.Domain.Classes;
using System.Security.Principal;

namespace NTB.SportsData.Api.Classes
{
    /// <summary>
    ///     Creating our own identity class
    /// </summary>
    public class ApiIdentity : IIdentity
    {
        /// <summary>
        ///     Gets or sets the Api User
        /// </summary>
        public ApiUser User { get; private set; }

        /// <summary>
        ///     Constructor with parameter
        /// </summary>
        /// <param name="user"></param>
        public ApiIdentity(ApiUser user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            this.User = user;
        }

        public string Name
        {
            get { return this.User.Username; }
        }

        public string AuthenticationType
        {
            get { return "Basic"; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }
    }
}