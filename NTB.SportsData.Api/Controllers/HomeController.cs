﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;

namespace NTB.SportsData.Api.Controllers
{
    public class HomeController : Controller
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(HomeController));

        public ActionResult Index()
        {
            Logger.Info("In Home Controller");
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
