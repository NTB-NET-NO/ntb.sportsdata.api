﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using log4net;
using NTB.SportsData.Api.Models.Matches;
using NTB.SportsData.Api.Models.XML;

namespace NTB.SportsData.Api.Controllers
{
    /// <summary>
    ///     Match controller is used to control the match api methods
    /// </summary>
    [Authorize]
    [RoutePrefix("api/match")]
    public class MatchController : ApiController
    {
        readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        ///     Constructor for Tournament Controller
        /// </summary>
        public MatchController()
        {
            _logger.Debug("In Match Controller");
        }

        /// <summary>
        ///     Method to use when you want a list of todays matches / events
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns>Data in either JSON or XML containing information about todays matches</returns>
        [HttpGet]
        public HttpResponseMessage GetTodayMatches(int id)
        {
            /**
             * Todo: Move JSON code to new class (Model)
             * Todo: Move XML code to new class (Model)
             * Todo: JSON shall be Sports in JSON
             * Todo: XML shall be SportsML
             */


            _logger.Info("Getting all tournaments for customer");
            var accept = Request.Headers.Accept.ToString();

            // var tournaments = new List<Tournament>();
            var matches = new MatchContext().GetTodaysMatches(id);
            if (matches == null)
            {
                return new HttpResponseMessage
                {
                    Content = new StringContent("Mo matches today", Encoding.UTF8, accept)
                };
            }
            string content;

            var modelTournament = new MatchModel();
            
            
            var xmlContent = modelTournament.SerializeObject(matches.ToList());

            if (accept == "application/json")
            {
                _logger.Info("Generating JSON");
                var model = new MatchModel();
                content = model.TransformObject(xmlContent);

                // content = model.SerializeObject(tournaments.ToList());
            }
            else
            {
                _logger.Info("Generating XML");


                content = modelTournament.TransformObject(xmlContent);

            }


            return new HttpResponseMessage
            {
                Content = new StringContent(content, Encoding.UTF8, accept)
            };




            // Now I have an XML-docoument that I can either XSLT into SportsML, or I can do something else with it.
            // todo: consider using XSLT to transform into SportsML
            // todo: consider using XSLT to transform into Sports in JSON

        }
    }
}
