﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Api.Interfaces;
using NTB.SportsData.Api.Models.Json;
using NTB.SportsData.Api.Models.Matches;
using NTB.SportsData.Api.Models.Tournaments;
using log4net;
using NTB.SportsData.Api.Models.XML;
using NTB.SportsData.Data.Team;
using NTB.SportsData.Data.Tournaments;

namespace NTB.SportsData.Api.Controllers
{
    /// <summary>
    ///     Tournament Controller is controlling all methods for getting Tournament Info
    /// </summary>
    [Authorize]
    [RoutePrefix("api/tournaments")]
    public class TournamentsController : ApiController
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentsController));

        /// <summary>
        ///     Constructor for Tournament Controller
        /// </summary>
        public TournamentsController()
        {
            Logger.Debug("In Tournament Controller");
        }

        /// <summary>
        ///     Hello Message is just a test method
        /// </summary>
        /// <returns>HttpResponseMessage containing bogus information</returns>
        [HttpGet]
        public HttpResponseMessage HelloMessage()
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode docNode = xmlDoc.CreateXmlDeclaration("1.0", "utf8", string.Empty);
            xmlDoc.AppendChild(docNode);

            XmlElement xmlRootElement = xmlDoc.CreateElement("hello");
            XmlElement xmlMainElement = xmlDoc.CreateElement("message");
            XmlText xmlText = xmlDoc.CreateTextNode("Hello from SportsData Api");
            xmlMainElement.AppendChild(xmlText);
            xmlRootElement.AppendChild(xmlMainElement);

            xmlDoc.AppendChild(xmlRootElement);

            return new HttpResponseMessage { Content = new StringContent(xmlDoc.OuterXml, Encoding.Unicode, "application/xml") }; 
        }

        /// <summary>
        ///     Get matches for the selected tournament
        /// </summary>
        /// <param name="id">the tournament id </param>
        /// <returns>Depending on your input, either a JSON array of matches or a SportsML-XML-structure</returns>
        [HttpGet]
        public HttpResponseMessage GetTournamentMatches(int id)
        {
            Logger.Info("Getting matches for tournament");
            var accept = Request.Headers.Accept.ToString();

            var matches = new MatchContext().GetAll(id);

            var model = new Models.XML.MatchModel();

            var xmlContent = model.SerializeObject(matches.ToList());


            string content;
            if (accept == "application/json")
            {
                Logger.Info("Generating JSON");
                var jsonModel = new Models.Json.MatchModel();

                content = jsonModel.ToString();
            }
            else
            {
                Logger.Info("Generating XML");
                

                if (matches == null)
                {
                    return null;
                }

                

                content = model.TransformObject(xmlContent);

            }


            return new HttpResponseMessage
            {
                Content = new StringContent(content, Encoding.UTF8, accept)
            };
        }

        
        //[Route("Get/{id:int}")]

        /// <summary>
        /// This method is used to get information about tournaments the customer subscribes to. The content in the returned data can be used to get information when using 
        /// the other methods in this API.
        /// We recommend you store this information in some database or as a file
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns>Depending on parameters this method will return either JSON or XML</returns>
        /// <code type="xml">
        /// <sports-event>
        ///         <event-metadata event-key="133990" event-name="2. div Kvinner avd 01" start-date-time="2013-04-01T00:00:00" end-date-time="2012-11-15T00:00:00">
        /// <sports-content-codes>
        /// <sports-content-code code-type="sport" code-name="Fotball" code-key="16"/>
        /// </sports-content-codes>
        /// </event-metadata>
        /// </sports-event>
        /// </code>
        /// <code type="json">
        /// {"events": [
        ///    {"1 div menn veteran 13/14": {
        ///        "competitionid": 319975,
        ///        "competitionname": "1 div menn veteran 13/14",
        ///        "competitionstartdate": "2013-10-22T00:00:00",
        ///        "competitionenddate": "2014-10-22T00:00:00",
        ///         "sportname": "soccer",
        ///         "sportcode": 16
        ///    }},
        ///    {"1. div Kvinner": {
        ///        "competitionid": 138981,
        ///        "competitionname": "1. div Kvinner",
        ///        "competitionstartdate": "2014-01-01T00:00:00",
        ///        "competitionenddate": "2014-12-31T00:00:00"
        ///        "sportname" : "handball",
        ///        "sportcode" : 23
        ///    }}
        ///]}
        /// </code>
        [HttpGet]
        public HttpResponseMessage GetCustomerTournaments(int id)
        {
            Logger.Info("Getting all tournaments for customer");
            var accept = Request.Headers.Accept.ToString();

            // var tournaments = new List<Tournament>();
            var tournaments = new TournamentContext().GetAll(id) ?? new List<Tournament>();

            var serializer = new ObjectSerializer();

            var xmlContent = serializer.SerializeObject(tournaments.ToList());

            ITransformationModel model;
            if (accept == "application/json")
            {
                Logger.Info("Generating JSON");
                model = new CompetitionModel();
            }
            else
            {
                Logger.Info("Generating XML");
                model = new TournamentModel();
            }

            string content = model.TransformObject(xmlContent);


            return new HttpResponseMessage
            {
                Content = new StringContent(content, Encoding.UTF8, accept)
            };

        }


        
        /// <summary>
        ///     Get information about the tournament by using its Id
        /// </summary>
        /// <param name="id">Integer value to be used to get tournament information</param>
        /// <returns>Returns information about the tournament</returns>
        [HttpGet]
        public HttpResponseMessage GetTournamentById(int id)
        {
            Logger.Info("Getting tournament number " + id);
            var accept = Request.Headers.Accept.ToString();

            var contentObject =  new TournamentContext().Get(id);

            var xmlContent = DataSerializer(contentObject);

            ITransformationModel model = GetTransformationModel(accept);

            model.XsltFile = ""; // This will throw an exception...
            var contentString = model.TransformObject(xmlContent);


            return new HttpResponseMessage
            {
                Content = new StringContent(contentString, Encoding.UTF8, accept)
            };
        }

        private string DataSerializer(object content) 
        {
            var serializer = new ObjectSerializer
            {
                SerializableObject = content
            };

            return serializer.Serializer();
        }

        /// <summary>
        ///     Get the model to be used to transform the data
        /// </summary>
        /// <param name="accept">Request Header Accept string</param>
        /// <returns>model object to be used to transform the serialized data</returns>
        private static ITransformationModel GetTransformationModel(string accept)
        {
            ITransformationModel model;
            if (accept == "application/json")
            {
                Logger.Info("Generating JSON");
                model = new CompetitionModel();
            }
            else
            {
                Logger.Info("Generating XML");
                model = new TournamentModel();
            }
            return model;
        }

        /// <summary>
        ///     Get a list of teams in the tournament from the specified tournament
        /// </summary>
        /// <param name="id">integer value to tell which tournament we are to get data from</param>
        /// <returns>List of teams</returns>
        public List<Team> GetTournamentTeamsAdvanced(int id)
        {
            Logger.Info("Getting Tournament Teams Advanced " + id);
            var accept = Request.Headers.Accept.ToString();


            var tournamentDataMapper = new TournamentDataMapper();
            var tournament = tournamentDataMapper.GetTournamentById(id);

            var teamRemoteDataMapper = new TeamRemoteDataMapper();
            return teamRemoteDataMapper.GetTournamentTeams(tournament);

        }

        /// <summary>
        ///     Get the standing for the selected tournament
        /// </summary>
        /// <param name="id">Tournament Id for the tournament you want to get data from</param>
        /// <returns>Either XML or JSON based on the header parameter</returns>
        public List<TournamentTable> GetTournamentStanding(int id)
        {
            Logger.Info("Getting tournament Standing " + id);
            var accept = Request.Headers.Accept.ToString();


            var tournamentDataMapper = new TournamentDataMapper();
            var tournament = tournamentDataMapper.GetTournamentById(id);

            var remoteDataMapper = new TournamentRemoteDataMapper();
            return remoteDataMapper.GetTournamentStandings(tournament);
        }
    }
}
