﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Api.Interfaces
{
    public interface IMatchDataMapper
    {
        IEnumerable<Match> GetAll(int tournamentId);
        Match Get(int id);
        /// <summary>
        ///     Get the customers matches for today
        /// </summary>
        /// <param name="customerId">integer holding the customer id</param>
        /// <returns>list of matches</returns>
        IEnumerable<Match> GetTodaysMatches(int customerId);
    }
}
