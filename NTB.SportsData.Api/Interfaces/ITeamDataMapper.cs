﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Api.Interfaces
{
    interface ITeamDataMapper
    {
        List<Team> GetTournamentTeams(int tournamentId, int sportId);
    }
}
