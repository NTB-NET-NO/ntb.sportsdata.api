﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Api.Interfaces
{
    interface ITournamentDataMapper
    {
        IEnumerable<Tournament> GetAll(int customerId);
        Tournament Get(int id);
    }
}
