﻿using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Api.Interfaces
{
    interface IUserDataMapper
    {
        ApiUser Get(string username, string password);
    }
}
