﻿using System;
using Glue;
using Glue.Converters;

namespace NTB.SportsData.Api.Mappers
{
    public abstract class BaseMapper<TLeftType, TRightType>
    {
        private readonly Mapping<TLeftType, TRightType> _map;

        protected BaseMapper()
            : this(null, null)
        {

        }
        protected BaseMapper(System.Func<TRightType, TLeftType> creatorTowardsLeft, System.Func<TLeftType, TRightType> creatorTowardsRight)
        {
            _map = new Mapping<TLeftType, TRightType>(creatorTowardsLeft, creatorTowardsRight);
            SetUpMapper(_map);
        }

        protected abstract void SetUpMapper(Mapping<TLeftType, TRightType> mapper);

        public virtual TRightType Map(TLeftType from, TRightType to)
        {
            return _map.Map(from, to);
        }

        public virtual TLeftType Map(TRightType from, TLeftType to)
        {
            return _map.Map(from, to);
        }

        public Mapping<TLeftType, TRightType> GetMapper()
        {
            return _map;
        }

        public void StringToIntConverter()
        {
            _map.AddConverter(Converting.BetweenIntAndString());
        }

        public IConverter NullableIntToIntConverter()
        {
            return new QuickConverter<int?, Int32>(ConvertNullableIntToInt32, ConvertInt32ToNull);
        }

        private int? ConvertInt32ToNull(int i)
        {
            if (i == 0)
            {
                return null;
            }

            return i;
        }

        private int ConvertNullableIntToInt32(int? i)
        {
            return i != null ? Convert.ToInt32(i) : 0;
        }

        public IConverter StringToDateTimeConverter()
        {
            return new QuickConverter<String, DateTime>(ConvertStringToDateTime, ConvertDateToString);
        }

        private string ConvertDateToString(DateTime dateTime)
        {
            return dateTime.ToLongDateString();
        }

        private DateTime ConvertStringToDateTime(string s)
        {
            return Convert.ToDateTime(s);
        }

        public IConverter StringToInt32Converter()
        {
            var converter = new QuickConverter<String, Int32>(ConvertStringToInt32, ConvertInt32ToString);

            return converter;
        }

        private string ConvertInt32ToString(int i)
        {
            return i.ToString();
        }

        private int ConvertStringToInt32(string s)
        {
            return s == string.Empty ? 0 : Convert.ToInt32(s);
        }

        public void YesNoConverter()
        {
            var converter = new QuickConverter<String, Boolean>(fromString => fromString != "N",
                                                             fromBool => fromBool ? "Y" : "N");

            // or ?
            _map.AddConverter(converter);
            // return converter;
        }

        public IConverter StringToTimeIntConverter()
        {
            var converter = new QuickConverter<string, Int32>(ConvertStringToTimeInt, ConvertFromTimeIntToString);

            return converter;
        }

        private string ConvertFromTimeIntToString(int i)
        {
            // A Time String is like this: 10:00:00
            var timeString = i.ToString();

            return timeString;
        }

        private int ConvertStringToTimeInt(string s)
        {
            var dt = Convert.ToDateTime(s);
            var stringTime = dt.ToShortTimeString().Replace(":", string.Empty);
            return Convert.ToInt32(stringTime);

        }
    }
}
