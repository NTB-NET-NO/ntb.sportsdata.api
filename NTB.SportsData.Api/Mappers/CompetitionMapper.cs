﻿using System.ComponentModel;
using Glue;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Domain.Classes.JSON;

namespace NTB.SportsData.Api.Mappers
{
    public class CompetitionMapper : BaseMapper<Tournament, Competition>
    {
        protected override void SetUpMapper(Mapping<Tournament, Competition> mapper)
        {
            mapper.Relate(x => x.TournamentId, y=>y.Competitionid);
            mapper.Relate(x => x.TournamentName, y => y.CompetitionName);
            mapper.Relate(x => x.SportName, y => y.SportName);
            mapper.Relate(x => x.SportId, y => y.SportId);
            mapper.Relate(x => x.SeasonId, y=>y.SeasonId);
            mapper.Relate(x => x.SeasonName, y => y.SeasonName);
            mapper.Relate(x => x.OrganizationId, y => y.OrganizationId, NullableIntToIntConverter());
            mapper.Relate(x => x.OrganizationName, y => y.OrganizationName);
            mapper.Relate(x => x.OrganizationNameShort, y => y.OrganizationNameShort);
            mapper.Relate(x => x.AgeCategoryId, y => y.AgeCategoryId, NullableIntToIntConverter());
            mapper.Relate(x => x.DisciplineId, y => y.DisciplineId, NullableIntToIntConverter());
            mapper.Relate(x => x.DisciplineName, y => y.DisciplineName);
        }
    }
}