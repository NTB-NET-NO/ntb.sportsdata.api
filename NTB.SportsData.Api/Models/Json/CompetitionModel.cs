﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Api.Interfaces;
using NTB.SportsData.Api.Mappers;

namespace NTB.SportsData.Api.Models.Json
{
    /// <summary>
    ///     Competition model is used to transform from RawXML to JSON
    /// </summary>
    public class CompetitionModel : ITransformationModel
    {
        /// <summary>
        ///     Constructor for the competition model class
        /// </summary>
        public CompetitionModel()
        {
            // Doing nothing at the moment
        }

        /// <summary>
        ///     Method transforming from raw to JSON
        /// </summary>
        /// <param name="document">String containing the XML structure</param>
        /// <returns>String containing json structure</returns>
        public string TransformObject(string document)
        {
            var transformer = new XslCompiledTransform();
            var xsltPath = ConfigurationManager.AppSettings["sportsmlxsltfilepath"];
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(document);

            transformer.Load(xsltPath + @"\sportsml-competition-json.xsl");

            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t" };
            settings.Encoding = Encoding.UTF8;
            settings.OmitXmlDeclaration = false;



            using (var textWriter = new StringWriter())
            {
                var writer = new XmlTextWriter(textWriter);

                transformer.Transform(xmlDoc, null, writer);

                return textWriter.ToString();
            }
            
            
        }

        public string XsltFile { get; set; }
    }
}