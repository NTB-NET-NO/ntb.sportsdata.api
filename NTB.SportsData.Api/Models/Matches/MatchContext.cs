﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;
using System.Linq;
using NTB.SportsData.Data.Matches;

namespace NTB.SportsData.Api.Models.Matches
{
    public class MatchContext : Interfaces.IMatchDataMapper
    {
        private Data.Interfaces.IMatchDataMapper _matchDataMapper;


        public IEnumerable<Match> GetAll(int tournamentId)
        {
            _matchDataMapper = new MatchDataMapper();
            var matches = _matchDataMapper.GetMatchesByTournamentId(tournamentId);

            return matches.AsEnumerable();
        }

        public Match Get(int id)
        {
            _matchDataMapper = new MatchDataMapper();

            var match = _matchDataMapper.GetMatchById(id);

            return match;
        }

        public IEnumerable<Match> GetTodaysMatches(int customerId)
        {
            _matchDataMapper = new MatchDataMapper();
            var matches = _matchDataMapper.GetTodaysMatchesByCustomerId(customerId);

            return matches;
        }
    }
}