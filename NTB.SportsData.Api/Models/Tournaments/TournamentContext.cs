﻿using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Api.Interfaces;
using NTB.SportsData.Data.Tournaments;

namespace NTB.SportsData.Api.Models.Tournaments
{
    
    public class TournamentContext : ITournamentDataMapper
    {
        
        public IEnumerable<Tournament> GetAll(int customerId)
        {
            var tournamentDataMapper = new TournamentDataMapper();
            var tournaments = tournamentDataMapper.GetTournamentsByCustomerId(customerId);

            return tournaments.AsEnumerable();
        }

        public Tournament Get(int id)
        {
            var tournamentDataMapper = new TournamentDataMapper();

            return tournamentDataMapper.GetTournamentById(id);
        }
    }
}