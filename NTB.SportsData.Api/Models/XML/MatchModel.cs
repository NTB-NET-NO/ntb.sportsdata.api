﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Api.Models.XML
{
    public class MatchModel
    {
        public MatchModel()
        {
            
        }

        public string TransformObject(string document)
        {
            var transformer = new XslCompiledTransform();
            var xsltPath = ConfigurationManager.AppSettings["sportsmlxsltfilepath"];
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(document);

            transformer.Load(xsltPath + @"\sportsml.xsl");

            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t" };
            settings.Encoding = Encoding.UTF8;
            settings.OmitXmlDeclaration = false;


            using (var textWriter = new StringWriter())
            {
                using (var writer = XmlWriter.Create(textWriter, settings))
                {
                    transformer.Transform(xmlDoc, writer);
                }

                return textWriter.ToString();
            }
        }

        public string SerializeObject(List<Match> matches)
        {
            var serializer = new XmlSerializer(typeof(List<Match>),
                        new XmlRootAttribute("Matches") { Namespace = string.Empty });

            var encoding = Encoding.UTF8;
            var contentStream = new StringWriter();
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add("", "");


            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t" };
            settings.Encoding = encoding;
            settings.OmitXmlDeclaration = false;


            using (var textWriter = new StringWriter())
            {
                using (var writer = XmlWriter.Create(textWriter, settings))
                {
                    // xDoc.CreateNavigator().AppendChild();

                    serializer.Serialize(writer, matches, namespaces);

                    // output = XmlWriter.Create(xDoc.OuterXml, settings);
                }

                return textWriter.ToString().Replace("utf-16", "utf-8");
            }

        }
    }
}