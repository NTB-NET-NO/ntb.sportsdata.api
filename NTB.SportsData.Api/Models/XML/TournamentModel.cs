﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Api.Interfaces;

namespace NTB.SportsData.Api.Models.XML
{
    /// <summary>
    ///     Turns Raw XML to SportsML for Tournaments
    /// </summary>
    public class TournamentModel : ITransformationModel
    {
        

        /// <summary>
        ///     Constructor for this
        /// </summary>
        public TournamentModel()
        {
            
            // Does not do much at the moment, but like to keep it
        }

        public string XsltFile { get; set; }

        public string TransformObject(string document)
        {
            if (string.IsNullOrEmpty(XsltFile))
            {
                throw new Exception("No XsltFile defined. Cannot continue");
            }
            var transformer = new XslCompiledTransform();
            var xsltPath = ConfigurationManager.AppSettings["sportsmlxsltfilepath"];
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(document);

            transformer.Load(xsltPath + @"\" + XsltFile);

            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t" };
            settings.Encoding = Encoding.UTF8;
            settings.OmitXmlDeclaration = false;


            using (var textWriter = new StringWriter())
            {
                using (var writer = XmlWriter.Create(textWriter, settings))
                {
                    transformer.Transform(xmlDoc, writer);
                }

                return textWriter.ToString();
            }
        }
    }
}